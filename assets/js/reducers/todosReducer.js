import listReducer from './todosReducer/listReducer';
import myTasksReducer from './todosReducer/myTasksReducer';
import commentsReducer from './todosReducer/commentsReducer';

const initialState = {
  list: [],
  myTasks: [],
  comments: {}
};

const todosReducer = (state = initialState, action) => ({
  list: listReducer(state.list, action),
  myTasks: myTasksReducer(state.myTasks, action),
  comments: commentsReducer(state.comments, action)
});

export default todosReducer;
