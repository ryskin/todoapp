import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import initDataReducer from './initDataReducer';
import generalDataReducer from './generalDataReducer';
import todosReducer from './todosReducer';
import projectsReducer from './projectsReducer';
import categoriesReducer from './categoriesReducer';
import popupReducer from './popupReducer';
import errorReducer from './errorsReducer';
import loaderReducer from './loaderReducer';
import usersReducer from './usersReducer';

const rootReducer = combineReducers({
  initData: initDataReducer,
  generalData: generalDataReducer,
  todos: todosReducer,
  projects: projectsReducer,
  categories: categoriesReducer,
  popup: popupReducer,
  loader: loaderReducer,
  errors: errorReducer,
  form: formReducer,
  users: usersReducer
});

export default rootReducer;
