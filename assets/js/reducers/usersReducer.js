import currentUserReducer from './usersReducer/currentUserReducer';
import listReducer from './usersReducer/listReducer';
import teamsReducer from './usersReducer/teamsReducer';
import teamMembersReducer from './usersReducer/teamMembersReducer';

const initialState = {
  currentUser: {},
  list: {},
  teams: {},
  teamMembers: {}
};

const usersReducer = (state = initialState, action) => ({
  currentUser: currentUserReducer(state.currentUser, action),
  list: listReducer(state.list, action),
  teams: teamsReducer(state.teams, action),
  teamMembers: teamMembersReducer(state.teamMembers, action),
});

export default usersReducer;
