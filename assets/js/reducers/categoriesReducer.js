import _ from 'lodash';

import * as constants from '../constants/categoryConstants';

const initialState = [];

const categoriesReducer = (state = initialState, action) => {
  let newState;
  const { payload, data, type } = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_CATEGORIES}_SUCCESS`: {
      newState = _(payload)
        .map((item, index) => ({
          ...item,
          selected: _.isEqual(index, 0),
        }))
        .sortBy(['order_num'])
        .value();
      break;
    }
    case constants.SELECT_CATEGORY: {
      newState = _.map(state, (item) => {
        let selected = false;
        if (_.isEqual(item.id, payload.id)) {
          selected = true;
        }
        return {
          ...item,
          selected,
        };
      });
      break;
    }
    case `${constants.ADD_CATEGORY}_SUCCESS`: {
      newState = [
        ...state,
        payload,
      ];
      break;
    }
    case `${constants.EDIT_CATEGORY}_SUCCESS`: {
      newState = _.map(state, item => (
        _.isEqual(item.id, payload.id) ? actionData : item
      ));
      break;
    }
    case `${constants.REMOVE_CATEGORY}_SUCCESS`: {
      newState = _(state)
        .filter(item => (
          !_.isEqual(item.id, actionData.id)
        ))
        .map((item, index) => {
          let selected = false;
          if (_.isEqual(index, 0)) {
            selected = true;
          }
          return {
            ...item,
            selected,
          };
        }).value();
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default categoriesReducer;
