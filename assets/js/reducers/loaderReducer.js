import _ from 'lodash';

const initialState = [];

const loaderReducer = (state = initialState, action) => {
  let newState;
  const {type} = action;

  if (_.includes(type, '_REQUEST')) {
    newState = [
      ...state,
      type,
    ];
  } else if (_.includes(type, '_SUCCESS')) {
    const reqType = _.replace(type, '_SUCCESS', '_REQUEST');
    newState = _.filter(state, item => !_.isEqual(item, reqType));
  } else if (_.includes(type, '_FAILURE')) {
    const reqType = _.replace(type, '_FAILURE', '_REQUEST');
    newState = _.filter(state, item => !_.isEqual(item, reqType));
  } else {
    newState = state;
  }

  return newState;
};

export default loaderReducer;
