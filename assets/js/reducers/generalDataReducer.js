import * as constants from '../constants/generalConstants';

const initialState = {
  viewMode: {
    isMobile: false,
    isTablet: false,
  }
};

const generalDataReducer = (state = initialState, action) => {
  let newState;
  const {payload, type} = action;

  switch (type) {
    case constants.UPDATE_VIEW_MODE: {
      newState = {
        ...state,
        viewMode: payload,
      };
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default generalDataReducer;
