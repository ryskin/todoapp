import listReducer from './projectsReducer/listReducer';
import statusesReducer from './projectsReducer/statusesReducer';

const initialState = {
  list: [],
  statuses: [],
};

const projectsReducer = (state = initialState, action) => ({
  list: listReducer(state.list, action),
  statuses: statusesReducer(state.statuses, action),
});

export default projectsReducer;
