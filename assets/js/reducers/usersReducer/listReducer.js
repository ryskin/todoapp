import _ from 'lodash';

import * as constants from '../../constants/userConstants';

const initialState = {};

const listReducer = (state = initialState, action) => {
  let newState;
  const {payload, type, data} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_USER_LIST}_SUCCESS`: {
      newState = _.reduce(payload, (memo, item) => ({
        ...memo,
        [item.id]: {
          ...item,
          title: item.nickname || item.name || item.email,
        },
      }), {});
      break;
    }
    case `${constants.ADD_USER}_SUCCESS`: {
      newState = {
        ...state,
        [payload.id]: {
          ...payload,
          title: payload.nickname || payload.name || payload.email,
        },
      };
      break;
    }
    case `${constants.EDIT_USER}_SUCCESS`: {
      newState = {
        ...state,
        [payload.id]: {
          ...payload,
          title: payload.nickname || payload.name || payload.email,
        },
      };
      break;
    }
    case `${constants.REMOVE_USER}_SUCCESS`: {
      newState = _.omit(state, [actionData.id]);
      break;
    }
    default:
      newState = state;
      break;
  }

  return newState;
};

export default listReducer;
