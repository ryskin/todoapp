import _ from 'lodash';

import * as constants from '../../constants/userConstants';

const initialState = {};

const teamMembersReducer = (state = initialState, action) => {
  const {payload, data: actionData, type} = action;

  switch (type) {
    case `${constants.LOAD_TEAM_MEMBERS}_SUCCESS`:
      state = _.reduce(payload, (memo, item) => ({
        ...memo,
        [item.id]: {
          team_id: item.team_id,
          user_id: item.user_id
        }
      }), {});
      break;

    case `${constants.ADD_TEAM_MEMBER}_SUCCESS`:
      state = _(state).set(
        [payload.id],
        {
          team_id: payload.team_id,
          user_id: payload.user_id
        }
      ).value();
      break;

    case `${constants.REMOVE_TEAM_MEMBER}_SUCCESS`:
      state = _.reduce(state, (result, item, key) => {
        if (key !== actionData.id) {
          result = {...result, [key]: item};
        }

        return result;
      }, {});
      break;
  }

  return state;
};

export default teamMembersReducer;
