import _ from 'lodash';

import {setStorage, removeStorage} from '../../helpers/Storage';
import * as constants from '../../constants/userConstants';

const initialState = {};

const currentUserReducer = (state = initialState, action) => {
  let newState;
  const {payload, header, type} = action;

  switch (type) {
    case `${constants.LOAD_USER}_SUCCESS`:
    case `${constants.SIGN_IN}_SUCCESS`: {
      newState = {
        ...payload.data,
      };

      _(header)
      .pick(['uid', 'client', 'access-token'])
      .forOwn((item, title) => {
        setStorage(title, item);
      });

      break;
    }
    case `${constants.SIGN_OUT}_SUCCESS`: {
      newState = {};
      removeStorage('uid');
      removeStorage('client');
      removeStorage('access-token');
      break;
    }
    default:
      newState = state;
      break;
  }

  return newState;
};

export default currentUserReducer;
