import _ from 'lodash';

import * as constants from '../../constants/userConstants';

const initialState = {};

const teamsReducer = (state = initialState, action) => {
  let newState;
  const {payload, data, type} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_TEAMS}_SUCCESS`: {
      newState = _.reduce(payload, (memo, item) => ({
        ...memo,
        [item.id]: {
          ...item,
        },
      }), {});
      break;
    }
    case `${constants.ADD_TEAM}_SUCCESS`: {
      newState = {
        ...state,
        [payload.id]: payload,
      };
      break;
    }
    case `${constants.REMOVE_TEAM}_SUCCESS`: {
      newState = _.omit(state, [actionData.id]);
      break;
    }
    case `${constants.EDIT_TEAM}_SUCCESS`: {
      newState = {
        ...state,
        [payload.id]: payload,
      };
      break;
    }
    case `${constants.ADD_TEAM_MEMBER}_SUCCESS`: {
      newState = {
        ...state,
        [payload.team_id]: {
          ...state[payload.team_id],
          team_members: [
            ...state[payload.team_id].team_members,
            {id: payload.user_id}
          ]
        },
      };
      break;
    }
    case `${constants.REMOVE_TEAM_MEMBER}_SUCCESS`: {

      newState = {
        ...state,
        [data.team_id]: {
          ...state[data.team_id],
          team_members: _.filter(state[data.team_id].team_members, (member) => !_.isEqual(member.id, data.user_id))
        },
      };
      break;
    }
    default:
      newState = state;
      break;
  }

  return newState;
};

export default teamsReducer;
