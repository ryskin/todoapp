import _ from 'lodash';

import * as constants from '../constants/errorConstants';

const initialState = [];

const errorsReducer = (state = initialState, action) => {
  let newState;
  const {error, type, data} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  if (!_.get(error, 'hideError', false) && _.includes(type, '_FAILURE')) {
    newState = [
      ...state,
      error,
    ];
  } else {
    switch (type) {
      case constants.REMOVE_ERROR: {
        newState = _.xor(state, [actionData])
        break;
      }
      default:
        newState = state;
        break;
    }
  }

  return newState;
};

export default errorsReducer;
