import * as constants from '../constants/popupConstants';

const initialState = {
  name: null,
};

const popupReducer = (state = initialState, action) => {
  let newState;
  const {payload, type} = action;

  switch (type) {
    case constants.OPEN_POPUP: {
      newState = {
        name: payload.name,
        data: payload.data || {},
      };
      break;
    }
    case constants.CLOSE_POPUP: {
      newState = {
        name: null,
        data: {},
      };
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default popupReducer;
