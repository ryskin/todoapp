import _ from 'lodash';

import * as userConstants from '../constants/userConstants';
import * as projectConstants from '../constants/projectConstants';
import * as categoryConstants from '../constants/categoryConstants';

const initialState = {
  [userConstants.LOAD_USER]: false,
  [userConstants.LOAD_USER_LIST]: false,
  [userConstants.LOAD_TEAMS]: false,
  [projectConstants.LOAD_PROJECTS]: false,
  [categoryConstants.LOAD_CATEGORIES]: false,
};

const initDataReducer = (state = initialState, action) => {
  let newState;
  const {type} = action;

  if (_.every(state, item => item)) {
    newState = state;
  } else {
    const newType = _(type)
    .replace('_FAILURE', '')
    .replace('_SUCCESS', '');

    if (_.includes(_.keys(state), newType)) {
      newState = {
        ...state,
        [newType]: true,
      };
    } else {
      newState = state;
    }
  }

  return newState;
};

export default initDataReducer;
