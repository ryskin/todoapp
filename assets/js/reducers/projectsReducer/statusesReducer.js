import _ from 'lodash';

import * as constants from '../../constants/projectConstants';

const initialState = [];

const statusesReducer = (state = initialState, action) => {
  let newState;
  const {payload, type} = action;

  switch (type) {
    case `${constants.LOAD_PROJECT_STATUSES}_SUCCESS`: {
      newState = _.map(payload, (id, name) => ({
        value: name,
        label: name,
      }));
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default statusesReducer;
