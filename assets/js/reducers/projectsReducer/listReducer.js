import _ from 'lodash';

import * as constants from '../../constants/projectConstants';

const initialState = [];

const listReducer = (state = initialState, action) => {
  let newState;
  const {payload, data, type} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_PROJECTS}_SUCCESS`: {
      const id = _.parseInt(actionData);
      newState = _(payload)
      .map(item => ({
        ...item,
        selected: _.isEqual(item.id, id),
      }))
      .sortBy(['order_num'])
      .value();
      break;
    }
    case constants.SELECT_PROJECT: {
      const id = _.parseInt(payload);
      newState = _.map(state, (item) => {
        let selected = false;
        if (_.isEqual(item.id, id)) {
          selected = true;
        }
        return {
          ...item,
          selected,
        };
      });
      break;
    }
    case `${constants.ADD_PROJECT}_SUCCESS`: {
      newState = [
        ...state,
        payload,
      ];
      break;
    }
    case `${constants.EDIT_PROJECT}_SUCCESS`: {
      newState = _.map(state, item => (
        _.isEqual(item.id, payload.id) ? actionData : item
      ));
      break;
    }
    case `${constants.REMOVE_PROJECT}_SUCCESS`: {
      newState = _(state)
      .filter(item => (
        !_.isEqual(item.id, actionData.id)
      ))
      .map((item, index) => {
        let selected = false;
        if (_.isEqual(index, 0)) {
          selected = true;
        }
        return {
          ...item,
          selected,
        };
      }).value();
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default listReducer;
