import _ from 'lodash';
import moment from 'moment';

import * as constants from '../../constants/toDoConstants';

const initialState = {};

const convertToLocalDate = date => moment(date).format();

const commentsReducer = (state = initialState, action) => {
  const {payload, data, type} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_COMMENTS}_SUCCESS`:
      const taskId = actionData.commentable_id || actionData.id;

      state = _(state)
      .update(taskId, () => {
        return _(payload)
        .map((item) => (
          {
            ...item,
            created_at: convertToLocalDate(item.created_at),
            updated_at: convertToLocalDate(item.updated_at)
          }
        ))
        .sortBy(['created_at'])
        .value();
      })
      .value();
      break;

    case `${constants.ADD_COMMENT}_SUCCESS`:
      state = _(state)
      .update(payload.commentable_id, (group) => _.concat(group, {
          ...payload,
          created_at: convertToLocalDate(payload.created_at),
          updated_at: convertToLocalDate(payload.updated_at)
        })
      )
      .value();
      break;

    case `${constants.REMOVE_COMMENT}_SUCCESS`:
      state = _(state)
      .update(actionData.commentable_id, (group) => _.filter(group,
        (item) => !_.isEqual(item.id, actionData.id)
      ))
      .value();
      break;
  }

  return state;
};

export default commentsReducer;
