import _ from 'lodash';

import * as constants from '../../constants/toDoConstants';
import * as projConstants from '../../constants/projectConstants';

const initialState = [];

const listReducer = (state = initialState, action) => {
  let newState;
  const {payload, data, type, hiddenData} = action;
  const actionData = (data && data.hasOwnProperty('data')) ? data.data.attributes : null;

  switch (type) {
    case `${constants.LOAD_TASKS}_SUCCESS`: {
      newState = _(payload)
      .map((item, index) => ({
        ...item,
        selected: _.isEqual(index, 0),
      }))
      .orderBy(['order_num', 'created_at'], ['asc', 'desc'])
      .value();
      break;
    }
    case `${constants.TOGGLE_TO_DO}_SUCCESS`: {
      newState = _.map(state, (item) => {
        let completed = item.completed;
        if (_.isEqual(item.id, payload.id)) {
          completed = payload.completed;
        }
        return {
          ...item,
          completed,
        };
      });
      break;
    }
    case `${constants.ADD_TO_DO}_SUCCESS`: {
      newState = [
        ...state,
        payload,
      ];
      break;
    }
    case `${constants.REMOVE_TO_DO}_SUCCESS`: {
      newState = _.filter(state, item => (
        !_.isEqual(item.id, actionData.id)
      ));
      break;
    }
    case `${constants.EDIT_TO_DO}_SUCCESS`: {
      newState = _.map(state, item => (
        _.isEqual(item.id, payload.id) ? payload : item
      ));
      break;
    }
    case `${projConstants.REMOVE_PROJECT}_SUCCESS`: {
      newState = _.filter(state, item => (
        !_.isEqual(item.project_id, actionData.id)
      ));
      break;
    }
    case `${constants.DELEGATE_TASK}_SUCCESS`: {
      newState = _.map(state, item => (
        _.isEqual(item.id, payload.id)
          ? payload
          : item
      ));
      break;
    }
    case `${constants.ATTACH_FILES}_SUCCESS`: {
      newState = _.map(state, (item) => {
        return _.isEqual(hiddenData.id, item.id)
          ? {
            ...item,
            files: [
              ...item.files,
              ...payload,
            ],
          } : item;
      });
      break;
    }
    case `${constants.REMOVE_FILE}_SUCCESS`: {
      newState = _.map(state, (item) => {
        return !_.isEqual(item.id, hiddenData.id)
          ? item
          : {
            ...item,
            files: payload,
          };
      });
      break;
    }
    default:
      newState = state;
      break;
  }
  return newState;
};

export default listReducer;
