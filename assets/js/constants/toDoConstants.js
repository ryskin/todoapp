export const LOAD_TASKS = 'LOAD_TASKS';
export const SORT_TO_DO_LIST = 'SORT_TO_DO_LIST';
export const TOGGLE_TO_DO = 'TOGGLE_TO_DO';
export const ADD_TO_DO = 'ADD_TO_DO';
export const REMOVE_TO_DO = 'REMOVE_TO_DO';
export const EDIT_TO_DO = 'EDIT_TO_DO';
export const DELEGATE_TASK = 'DELEGATE_TASK';
export const ATTACH_FILES = 'ATTACH_FILES';
export const REMOVE_FILE = 'REMOVE_FILE';
export const LOAD_MY_TASKS = 'LOAD_MY_TASKS';

export const TASK_TYPE = 'TASK_TYPE';
export const ACTIVE_TASK_TYPE = 'ACTIVE_TASK_TYPE';
export const COMPLETED_TASK_TYPE = 'COMPLETED_TASK_TYPE';
export const DND_DIRECTION_UP = 'DND_DIRECTION_UP';
export const DND_DIRECTION_DOWN = 'DND_DIRECTION_DOWN';
export const DND_DIRECTION_INSIDE = 'DND_DIRECTION_INSIDE';

export const LOAD_COMMENTS = 'LOAD_COMMENTS';
export const ADD_COMMENT = 'ADD_COMMENT';
export const REMOVE_COMMENT = 'REMOVE_COMMENT';
