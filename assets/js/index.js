import removeHover from 'remove-hover';
import 'moment/locale/ru';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './stores';

import AppContainer from './containers/AppContainer';

const isProd = (process.env.NODE_ENV === 'production');
removeHover();
const store = configureStore(isProd);

ReactDOM.render(
  <Provider store={store}>
    <AppContainer isProd={isProd} />
  </Provider>
  , document.querySelector('.app'));
