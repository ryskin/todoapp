import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as userActions from '../actions/userActions';
import * as projectActions from '../actions/projectActions';
import * as categoryActions from '../actions/categoryActions';

import { getStorage } from '../helpers/Storage';

import App from '../components/App';

class AppContainer extends Component {
  componentDidMount() {
    this.props.userActions.loadUser({
      uid: getStorage('uid'),
      'access-token': getStorage('access-token'),
      client: getStorage('client'),
    });
    this.props.projectActions.loadProjects();
    this.props.categoryActions.loadCategories();
    this.props.userActions.loadUserList();
    this.props.userActions.loadTeams();
    this.props.userActions.loadTeamMembers();
  }

  shouldComponentUpdate(nextProps) {
    return !_.isEqual(this.props.isLoadedInitData, nextProps.isLoadedInitData);
  }

  render() {
    return (<App isLoadedInitData={this.props.isLoadedInitData} isProd={this.props.isProd} />);
  }
}

AppContainer.propTypes = {
	isProd: PropTypes.bool.isRequired,
  isLoadedInitData: PropTypes.bool.isRequired,
  userActions: PropTypes.object.isRequired,
  projectActions: PropTypes.object.isRequired,
  categoryActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    isLoadedInitData: _.every(state.initData, item => item),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    projectActions: bindActionCreators(projectActions, dispatch),
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
