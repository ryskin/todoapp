import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {reset as formResetAction} from 'redux-form';

import {
  loadComments as loadCommentsAction,
  addComment as addCommentAction,
  removeComment as removeCommentAction
} from '../../actions/toDoListActions';

import validationHook from '../../actions/actionCreatorHooks/validateHookPromise';

import TaskCommentsComponent from '../../components/todos/ToDoList/TaskList/TaskItem/TaskComments';
import {TASK_COMMENT_FORM} from '../../components/todos/ToDoList/TaskList/TaskItem/Comments/CommentForm';
import TaskFormValidation from '../../components/todos/ToDoList/TaskList/TaskItem/Comments/CommentFormValidation';

class TaskCommentsContainer extends Component {

  getChildContext() {
    return {
      getUserInfo: (userId) => _.get(this.props.userList, userId, null)
    };
  }

  componentDidMount() {
    const {task, loadCommentsAction} = this.props;
    loadCommentsAction(task);
  }

  addComment = (comment) => {
    const {task, addCommentAction, formResetAction} = this.props;

    addCommentAction({...comment, commentable_id: task.id});
    formResetAction(TASK_COMMENT_FORM);
  };

  removeComment = (comment) => (e) => {
    e.preventDefault();

    this.props.removeCommentAction(comment);
  };

  render() {
    const {task, comments, currentUser} = this.props;
    const props = {
      task,
      comments,
      currentUser,
      addCommentAction: validationHook(TaskFormValidation, this.addComment),
      removeCommentAction: this.removeComment
    };

    return (
      <TaskCommentsComponent {...props} />
    );
  }
}

TaskCommentsContainer.childContextTypes = {
  getUserInfo: PropTypes.func.isRequired
};

TaskCommentsContainer.propTypes = {
  task: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  userList: PropTypes.object.isRequired,
  loadCommentsAction: PropTypes.func.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  removeCommentAction: PropTypes.func.isRequired,
  formResetAction: PropTypes.func.isRequired
};

const mapStateToProps = (state, props) => {
  const {currentUser, list: userList} = state.users;
  const {task} = props;
  const comments = _.get(state.todos.comments, task.id, []);

  return {
    currentUser,
    userList,
    task,
    comments
  };
};

const mapDispatchToProps = {
  loadCommentsAction,
  addCommentAction,
  removeCommentAction,
  formResetAction
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskCommentsContainer);
