import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import AddProjectContainer from './Popup/AddProjectContainer';
import AddTeamContainer from './Popup/AddTeamContainer';
import DelegateTaskContainer from './Popup/DelegateTaskContainer';
import AddTeamMemberContainer from './Popup/AddTeamMemberContainer';
import AddUserContainer from './Popup/AddUserContainer';

class PopupContainer extends Component {
  render() {
    let container;
    const { name } = this.props.popup;

    switch (name) {
      case 'add-project':
        container = (
          <AddProjectContainer />
        );
        break;
      case 'delegate-task':
        container = (
          <DelegateTaskContainer />
        );
        break;
      case 'add-team':
        container = (
          <AddTeamContainer />
        );
        break;
      case 'add-team-member':
        container = (
          <AddTeamMemberContainer />
        );
        break;
      case 'add-user':
        container = (
          <AddUserContainer />
        );
        break;
      default:
        container = null;
        break;
    }
    return container;
  }
}

PopupContainer.propTypes = {
  popup: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    popup: state.popup,
  };
}

export default connect(mapStateToProps, {})(PopupContainer);
