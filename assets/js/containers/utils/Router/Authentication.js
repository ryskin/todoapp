import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

export default function (ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if (!this.props.authenticated) {
        this.context.router.push('/login');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authenticated) {
        this.context.router.push('/login');
      }
    }

    render() {
      return !this.props.authenticated
        ? null
        : (<ComposedComponent {...this.props} />);
    }
  }

  Authentication.contextTypes = {
    router: PropTypes.object,
  };

  Authentication.propTypes = {
    authenticated: PropTypes.bool,
  };

  function mapStateToProps(state) {
    return { authenticated: !_.isEmpty(state.users.currentUser) };
  }

  return connect(mapStateToProps)(Authentication);
}
