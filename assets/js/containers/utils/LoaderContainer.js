import { connect } from 'react-redux';

import Loader from '../../components/utils/Loader';

function mapStateToProps(state) {
  return {
    loader: state.loader,
  };
}

export default connect(mapStateToProps, {})(Loader);
