import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as generalActions from '../../actions/generalActions';

import Layout from '../../components/utils/Layout';

class LayoutContainer extends Component {

//---------------------------------------------------------------------------
// LIFECYCLE
//---------------------------------------------------------------------------
  componentDidMount() {
    this.props.generalActions.updateViewMode({
      isTablet: this._getIsTablet(),
      isMobile: this._getIsMobile(),
    })
  }
  componentWillMount() {
    window.addEventListener('resize', this._onWindowResize);
  }

  componentWillUnmount() {
    window.addEventListener('resize', this._onWindowResize);
  }

//---------------------------------------------------------------------------
// INNER FUNCTIONS
//---------------------------------------------------------------------------

  _onWindowResize = () => {
    const isMobile = this._getIsMobile();
    const isTablet = this._getIsTablet();

    if (!_.isEqual(isMobile, this.props.viewMode.isMobile)
      || !_.isEqual(isTablet, this.props.viewMode.isTablet)) {
      this.props.generalActions.updateViewMode({
        isTablet,
        isMobile,
      })
    }
  };

  _getIsMobile = () =>
    (window.innerWidth <= 768);

  _getIsTablet = () =>
    (window.innerWidth > 768 && window.innerWidth <= 1200);

//---------------------------------------------------------------------------
// RENDER
//---------------------------------------------------------------------------

  render() {
    const route = _.last(this.props.routes);
    return (
      <Layout routeName={_.get(route, 'name', '')}>
        {this.props.children}
      </Layout>
    );
  }
}

LayoutContainer.propTypes = {
  children: PropTypes.element,
  generalActions: PropTypes.object.isRequired,
  viewMode: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    viewMode: state.generalData.viewMode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    generalActions: bindActionCreators(generalActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LayoutContainer);
