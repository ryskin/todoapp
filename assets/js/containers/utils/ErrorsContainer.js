import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Errors from '../../components/utils/Errors';

import * as errorActions from '../../actions/errorActions';

class ErrorsContainer extends Component {

  removeError = (item) => {
    this.props.errorActions.removeError(item);
  };

  render() {
    const { errors } = this.props;
    const props = {
      errors,
      removeError: this.removeError,
    };

    return (
      <Errors {...props}/>
    );
  }
}

ErrorsContainer.propTypes = {
  errors: PropTypes.array.isRequired,
  errorActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    errors: state.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
		errorActions: bindActionCreators(errorActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorsContainer);
