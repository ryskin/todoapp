import React from 'react';
import { Router, Route, Redirect, IndexRedirect, browserHistory } from 'react-router';

import requireAuth from './Router/Authentication';

import LayoutContainer from './LayoutContainer';

import Login from '../pages/Login';
import Todos from '../pages/Todos';
import MyTasks from '../pages/MyTasks';

import UserManagement from '../pages/UserManagement';
import Teams from '../pages/UserManagement/Teams';
import Users from '../pages/UserManagement/Users';

const RouterContainer = () => (
  <Router history={browserHistory}>
    <Route component={LayoutContainer}>
      <Redirect from="/" to="/todos" />
      <Route path="/login" component={Login} />
      <Route name="todos" path="/todos(/:projectId)" component={requireAuth(Todos)} />
      <Route name="my-tasks" path="/my-tasks" component={requireAuth(MyTasks)} />
      <Route name="user-management" path="/user-management" component={requireAuth(UserManagement)}>
        <IndexRedirect to="teams" />
        <Route name="user-management/teams" path="teams" component={Teams} />
        <Route name="user-management/users" path="users" component={Users} />
      </Route>
    </Route>
  </Router>
);

export default RouterContainer;
