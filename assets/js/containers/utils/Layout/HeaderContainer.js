import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as userActions from '../../../actions/userActions';

import Header from '../../../components/utils/layout/Header';

class HeaderContainer extends Component {
  toSignIn = () =>
    this.context.router.push('/login');

  toAdminstration = () =>
    this.context.router.push('/user-management');

  toMyTasks = () =>
    this.context.router.push('/my-tasks');

  toTodos = () =>
    this.context.router.push('/todos');

  signOut = () =>
    this.props.userActions.signOut();

  toggleButton = () => {
    if (_.isEmpty(this.props.user)) {
      this.toSignIn();
    } else {
      this.signOut();
    }
  };

  render() {
    const { user } = this.props;

    const props = {
      user,
      toggleButton: this.toggleButton,
      toMyTasks: this.toMyTasks,
      toTodos: this.toTodos,
      toAdminstration: this.toAdminstration,
      routeName: this.props.routeName,
    };

    return (
      <Header {...props} />
    );
  }
}

HeaderContainer.contextTypes = {
  router: PropTypes.object.isRequired,
};

HeaderContainer.propTypes = {
  userActions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  routeName: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.users.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
