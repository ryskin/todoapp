import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reset, initialize } from 'redux-form';
import _ from 'lodash';

import * as popupActions from '../../../actions/popupActions';
import * as userActions from '../../../actions/userActions';

import AddUser from '../../../components/utils/popup/AddUser';

class AddUserContainer extends Component {
  componentDidMount() {
    if (!_.isEmpty(this.props.data)) {
      this.props.initializeAction('addUser', this.props.data);
    }
  }

  removeUser = (item) => {
    this.props.userActions.removeUser(item);
    this.closePopup();
  };

  closePopup = () => {
    this.props.popupActions.closePopup();
  };

  addUser = (data) => {
    this.props.userActions.addUser(data);
    this._clearForm();
  };

  editUser = (data) => {
    this.props.userActions.editUser(
      _.omit(data, ['created_at', 'updated_at', 'title', 'selected', 'uid', 'provider'])
    );
    this._clearForm();
  };

  _clearForm = () => {
    this.closePopup();
    this.props.resetAction('addUser');
  };

  render() {
    const { data, workMode } = this.props;

    const props = {
      workMode,
      data,
      close: this.closePopup,
      submitData: _.isEqual(workMode, 'create') ? this.addUser : this.editUser,
      removeUser: this.removeUser,
    };

    return (
      <AddUser {...props} />
    );
  }
}

AddUserContainer.propTypes = {
  popupActions: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  resetAction: PropTypes.func.isRequired,
  initializeAction: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  workMode: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    data: state.popup.data,
    workMode: _.isEmpty(state.popup.data)
      ? 'create'
      : 'edit',
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    resetAction: bindActionCreators(reset, dispatch),
    initializeAction: bindActionCreators(initialize, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUserContainer);
