import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reset, initialize } from 'redux-form';
import _ from 'lodash';

import * as popupActions from '../../../actions/popupActions';
import * as userActions from '../../../actions/userActions';

import AddTeam from '../../../components/utils/popup/AddTeam';

class AddTeamContainer extends Component {
  componentDidMount() {
    if (!_.isEmpty(this.props.data)) {
      this.props.initializeAction('addTeam', this.props.data);
    }
  }

  removeTeam = (item) => {
    this.props.userActions.removeTeam(item);
    this.closePopup();
  };

  closePopup = () => {
    this.props.popupActions.closePopup();
  };

  addTeam = (data) => {
    this.props.userActions.addTeam(data);
    this._clearForm();
  };

  editTeam = (data) => {
    this.props.userActions.editTeam(data);
    this._clearForm();
  };

  _clearForm = () => {
    this.closePopup();
    this.props.resetAction('addTeam');
  }

  render() {
    const { data, workMode } = this.props;

    const props = {
      workMode,
      data,
      close: this.closePopup,
      submitData: _.isEqual(workMode, 'create') ? this.addTeam : this.editTeam,
      removeTeam: this.removeTeam,
    };

    return (
      <AddTeam {...props} />
    );
  }
}

AddTeamContainer.propTypes = {
  popupActions: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  resetAction: PropTypes.func.isRequired,
  initializeAction: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  workMode: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    data: state.popup.data,
    workMode: _.isEmpty(state.popup.data)
      ? 'create'
      : 'edit',
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    resetAction: bindActionCreators(reset, dispatch),
    initializeAction: bindActionCreators(initialize, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTeamContainer);
