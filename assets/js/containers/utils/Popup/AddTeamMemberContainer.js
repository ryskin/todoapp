import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as popupActions from '../../../actions/popupActions';
import * as userActions from '../../../actions/userActions';

import SelectUser from '../../../components/utils/popup/Common/SelectUser';

class AddTeamMemberContainer extends Component {
  addTeamMember = user => () => {
    this.props.popupActions.closePopup();
    this.props.userActions.addTeamMember({
      user_id: user.id,
      team_id: this.props.data.id,
    });
  };

  closePopup = () => {
    this.props.popupActions.closePopup();
  };

  render() {
    const { users } = this.props;

    const props = {
      title: 'Добавить пользователя в команду',
      description: 'Выберите пользователя из списка, для добавления его в команду:',
      users,
      selectUser: this.addTeamMember,
      close: this.closePopup,
    };

    return (
      <SelectUser {...props} />
    );
  }
}

AddTeamMemberContainer.propTypes = {
  data: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const teamMembersIds = _.map(state.popup.data.team_members, member => (member.id));
  const users = _.pickBy(state.users.list, user => !_.includes(teamMembersIds, user.id));

  return {
    users,
    data: state.popup.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTeamMemberContainer);
