import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import {reset as resetAction, initialize as initializeAction} from 'redux-form';

import validationHook from '../../../actions/actionCreatorHooks/validateHookPromise';
import validate from '../../../components/utils/popup/ProjectValidation';

import * as popupActions from '../../../actions/popupActions';
import * as projectActions from '../../../actions/projectActions';

import AddProject, {PROJECT_FORM_NAME} from '../../../components/utils/popup/AddProject';

class AddProjectContainer extends Component {

  componentDidMount() {
    const {data, initializeFormAction} = this.props;

    if (!_.isEmpty(data)) {
      const {id, name, url, description, status, category_id} = data;
      const teams_ids = _.isEmpty(data['teams_ids']) ? [] : _.map(data['teams_ids'], (value) => Number(value));
      const item = {id, name, url, description, status, category_id, team_ids: teams_ids};

      initializeFormAction(PROJECT_FORM_NAME, item);
    }
  }

  removeProject = () => {
    const {
      data,
      projectActions: {removeProject},
      popupActions: {closePopup},
      resetFormAction
      } = this.props;

    removeProject(data);
    closePopup();
    resetFormAction(PROJECT_FORM_NAME);
  };

  saveProject = (data) => {
    const {
      workMode,
      projectActions: {addProject, editProject},
      popupActions: {closePopup},
      resetFormAction
      } = this.props;

    if (_.isEqual(workMode, 'create')) {
      addProject(data);
    } else {
      editProject(data);
    }

    closePopup();
    resetFormAction(PROJECT_FORM_NAME);
  };

  closePopup = () => {
    const {popupActions: {closePopup}, resetFormAction} = this.props;
    closePopup();
    resetFormAction(PROJECT_FORM_NAME);
  };

  render() {
    const { categories, teams, data, workMode, statuses, popupActions: {closePopup} } = this.props;

    const props = {
      categories,
      teams,
      workMode,
      data,
      statuses,
      closePopupAction: closePopup,
      saveProjectAction: validationHook(validate, this.saveProject),
      removeProjectAction: this.removeProject
    };

    return (
      <AddProject {...props} />
    );
  }
}

AddProjectContainer.propTypes = {
  categories: PropTypes.object.isRequired,
  teams: PropTypes.object.isRequired,
  statuses: PropTypes.array.isRequired,
  data: PropTypes.object.isRequired,
  workMode: PropTypes.string.isRequired,
  projectActions: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
  resetFormAction: PropTypes.func.isRequired,
  initializeFormAction: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  const resetValue = {label: '', value: ''};

  const categoryOptions = _.map(state.categories, (item) => ({label: item.name, value: Number(item.id)}));
  //categoryOptions.push(resetValue);

  const teamsOptions = _.map(state.users.teams, (item) => ({label: item.name, value: Number(item.id)}));
  //teamsOptions.push(resetValue);

  return {
    categories: {
      resetValue,
      options: categoryOptions
    },
    teams: {
      resetValue,
      options: teamsOptions
    },
    statuses: state.projects.statuses,
    data: state.popup.data,
    workMode: _.isEmpty(state.popup.data) ? 'create' : 'edit'
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    projectActions: bindActionCreators(projectActions, dispatch),
    resetFormAction: bindActionCreators(resetAction, dispatch),
    initializeFormAction: bindActionCreators(initializeAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectContainer);
