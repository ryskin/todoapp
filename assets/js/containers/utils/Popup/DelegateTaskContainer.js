import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as popupActions from '../../../actions/popupActions';
import * as toDoListActions from '../../../actions/toDoListActions';

import SelectUser from '../../../components/utils/popup/Common/SelectUser';

class DelegateTaskContainer extends Component {
  delegateTask = item => () => {
    this.props.popupActions.closePopup();
    this.props.toDoListActions.delegateTask({
      ...this.props.data,
      assignee_id: item.id,
    });
  }

  closePopup = () => {
    this.props.popupActions.closePopup();
  };

  render() {
    const { users } = this.props;

    const props = {
      title: 'Делегирование задачи',
      description: 'Выберите пользователя из списка, на кого вы хотите назначить задачу:',
      users,
      selectUser: this.delegateTask,
      close: this.closePopup,
    };

    return (
      <SelectUser {...props} />
    );
  }
}

DelegateTaskContainer.propTypes = {
  data: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
  toDoListActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    users: state.users.list,
    data: state.popup.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    toDoListActions: bindActionCreators(toDoListActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DelegateTaskContainer);
