import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as popupActions from '../../../actions/popupActions';

import ProjectsComponent from '../../../components/todos/Projects';

class Projects extends Component {
  openAddProjectPopup = () => {
    this.props.popupActions.openPopup({
      name: 'add-project',
    });
  };

  editProject = (data) => {
    this.props.popupActions.openPopup({
      name: 'add-project',
      data,
    });
  };

  render() {
    const { projects, categories } = this.props;

    const props = {
      projects,
      categories,
      addProject: this.openAddProjectPopup,
      selectProject: this.selectProject,
      editProject: this.editProject,
      removeProject: this.removeProject,
    };

    return (
      <ProjectsComponent {...props} />
    );
  }
}

Projects.propTypes = {
  popupActions: PropTypes.object.isRequired,
  projects: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const selectedProject = _(state.projects.list)
    .filter(item => (item.selected))
    .head();

  const projects = _.reduce(state.projects.list, (memo, item) => {
    const currentItem = !_.isUndefined(memo[item.category_id]) ? memo[item.category_id] : [];
    return {
      ...memo,
      [_.get(item, 'category_id' , -1)]: [
        ...currentItem,
        item,
      ],
    };
  }, {});

  const categories = _.reduce(state.categories, (memo, item) => ({
    ...memo,
    [item.id]: item,
  }), {});

  return {
    projects,
    categories,
    selectedProject,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
