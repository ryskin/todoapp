import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reset } from 'redux-form';
import _ from 'lodash';

import * as toDoListActions from '../../../actions/toDoListActions';
import * as popupActions from '../../../actions/popupActions';

import ToDoListComponent from '../../../components/todos/ToDoList';

class ToDoList extends Component {
  toggleCompleted = item => (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.toDoListActions.toggleToDo({
      ...item,
      completed: !item.completed,
    });
  };

  removeTask = item => (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.toDoListActions.removeToDo(item);
  };

  editAndRefreshTask = item => (data) => {
    this.props.toDoListActions.editAndRefreshToDo({
      ...item,
      name: data.name || item.name,
      description: !_.isUndefined(data.description)
        ? data.description
        : item.description,
    });
  };

  editTask = item => (data) => {
    this.props.toDoListActions.editToDo({
      ...item,
      name: data.name || item.name,
      description: !_.isUndefined(data.description)
        ? data.description
        : item.description,
    });
  };

  addTask = (data) => {
    this.props.toDoListActions.addToDo({
      name: data.name,
      project_id: this.props.selectedProject.id,
    });
    this.props.reset('addTask');
  };

  addSubtask = item => (data) => {
    this.props.toDoListActions.addToDo({
      name: data.name,
      project_id: this.props.selectedProject.id,
      parent_id: item.id,
    });
  };

  attachImageToTask = task => (files) => {
    this.props.toDoListActions.attachFiles(task, files);
  };

  removeImageFromTask = task => (file) => {
    this.props.toDoListActions.removeFile(task, file);
  };

  delegateTask = item => (event) => {
    event.stopPropagation();
    event.preventDefault();

    if (!item.assignee_id) {
      this.props.popupActions.openPopup({
        name: 'delegate-task',
        data: item,
      });
    } else {
      this.props.toDoListActions.delegateTask({
        ...item,
        assignee_id: null,
      });
    }
  };

  render() {
    const { selectedProject, users, tasks } = this.props;

    const props = {
      tasks,
      selectedProject,
      users,
      toggleCompleted: this.toggleCompleted,
      removeTask: this.removeTask,
      editTask: this.editTask,
      editAndRefreshTask: this.editAndRefreshTask,
      addTask: this.addTask,
      delegateTask: this.delegateTask,
      addSubtask: this.addSubtask,
      attachImageToTask: this.attachImageToTask,
      removeImageFromTask: this.removeImageFromTask,
    };

    return (
      <ToDoListComponent {...props} />
    );
  }
}

ToDoList.propTypes = {
  toDoListActions: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
  tasks: PropTypes.object.isRequired,
  selectedProject: PropTypes.object,
  reset: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const selectedProject = _(state.projects.list)
    .filter(item => (item.selected))
    .head();

  const todos = !_.isUndefined(selectedProject)
    ? _.filter(state.todos.list, item =>
    _.isEqual(item.project_id, selectedProject.id)
  ) : [];

  return {
    tasks: {
      completed: _.filter(todos, item => item.completed && !_.isNumber(item.parent_id)),
      incompleted: _.filter(todos, item => !item.completed && !_.isNumber(item.parent_id)),
      childrens: _(todos)
        .filter(item => _.isNumber(item.parent_id))
        .reduce((memo, item) => ({
          ...memo,
          [item.parent_id]: _.concat(
            _.isUndefined(memo[item.parent_id]) ? [] : memo[item.parent_id],
            item
          ),
        }), {}),
    },
    selectedProject,
    users: state.users.list,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toDoListActions: bindActionCreators(toDoListActions, dispatch),
    popupActions: bindActionCreators(popupActions, dispatch),
    reset: bindActionCreators(reset, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
