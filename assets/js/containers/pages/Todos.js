import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import scrollToElement from 'scroll-to-element';
import _ from 'lodash';

import * as projectActions from '../../actions/projectActions';
import * as toDoListActions from '../../actions/toDoListActions';

import TodosComponent from '../../components/todos/Todos';

class Todos extends Component {
  componentDidMount() {
    this.props.projectActions.loadStatuses();
    this.props.toDoListActions.loadTasks();

    if (!_.isUndefined(this.props.params.projectId)) {
      this.props.projectActions.selectProject(this.props.params.projectId);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(nextProps.params.projectId, this.props.params.projectId)) {
      scrollToElement('.app-wrapper', {
        duration: 300,
      });
      this.props.projectActions.selectProject(nextProps.params.projectId);
    }
  }

  render() {
    return (
      <TodosComponent {...this.props} />
    );
  }
}

Todos.propTypes = {
  projectActions: PropTypes.object.isRequired,
  toDoListActions: PropTypes.object.isRequired,
  params: PropTypes.object,
  isMobile: PropTypes.bool.isRequired,
  isTablet: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state.generalData.viewMode,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    projectActions: bindActionCreators(projectActions, dispatch),
    toDoListActions: bindActionCreators(toDoListActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
