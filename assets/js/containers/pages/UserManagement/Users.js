import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as popupActions from '../../../actions/popupActions';

import UsersComponent from '../../../components/userManagement/Users';

class Users extends Component {
  addUser = () => {
    this.props.popupActions.openPopup({
      name: 'add-user',
    });
  };

  editUser = (data) => {
    this.props.popupActions.openPopup({
      name: 'add-user',
      data,
    });
  };

  render() {
    const props = {
      users: this.props.users,
      addUser: this.addUser,
      editUser: this.editUser,
    };

    return (
      <UsersComponent {...props} />
    );
  }
}

Users.propTypes = {
  users: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    users: state.users.list,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
