import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as popupActions from '../../../actions/popupActions';
import * as userActions from '../../../actions/userActions';

import TeamsComponent from '../../../components/userManagement/Teams';

class Teams extends Component {
  addTeam = () => {
    this.props.popupActions.openPopup({
      name: 'add-team',
    });
  };

  addTeamMember = (data) => {
    this.props.popupActions.openPopup({
      name: 'add-team-member',
      data,
    });
  };

  removeTeamMember = (team, user) => () => {
    const {teamMembers, userActions: {removeTeamMember}} = this.props;
    const removingData = {
      team_id: team.id,
      user_id: user.id,
    };

    const memberId = _.findKey(teamMembers, removingData);
    console.debug(teamMembers, removingData);
    if (!_.isUndefined(memberId)) {
      removeTeamMember({...removingData, id: memberId})
    }
  };

  editTeam = (data) => {
    this.props.popupActions.openPopup({
      name: 'add-team',
      data,
    });
  };

  render() {
    const props = {
      teams: this.props.teams,
      users: this.props.users,
      addTeam: this.addTeam,
      addTeamMember: this.addTeamMember,
      removeTeamMember: this.removeTeamMember,
      editTeam: this.editTeam,
    };

    return (
      <TeamsComponent {...props} />
    );
  }
}

Teams.propTypes = {
  teams: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  popupActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    teams: state.users.teams,
    users: state.users.list,
    teamMembers: state.users.teamMembers,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popupActions: bindActionCreators(popupActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Teams);
