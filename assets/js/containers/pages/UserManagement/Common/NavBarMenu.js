import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import TabsHeader from '../../../../components/utils/layout/TabsHeader';

class NavBarMenu extends Component {

  render() {
    const props = {
      title: 'Управление',
      tabs: [{
        title: 'Команды',
        link: '/user-management/teams',
      }, {
        title: 'Пользователи',
        link: '/user-management/users',
      }],
      routeName: this.props.routeName,
      isMobile: this.props.isMobile,
      isTablet: this.props.isTablet,
    };

    return (
      <TabsHeader {...props} />
    );
  }
}

NavBarMenu.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  isTablet: PropTypes.bool.isRequired,
};

NavBarMenu.contextTypes = {
  router: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state.generalData.viewMode,
  };
}

export default connect(mapStateToProps, {})(NavBarMenu);
