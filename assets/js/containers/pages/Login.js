import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as userActions from '../../actions/userActions';

import LoginComponent from '../../components/login/Login';

class Login extends Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (!_.isEmpty(nextProps.user)) {
      this.redirect(nextProps.user, nextContext.router);
    }
  }

  redirect = (user, router) => {
    if (!_.isEmpty(user)) {
      router.push('/todos');
    }
  };

  signIn = data =>
    this.props.userActions.signIn(data);

  render() {
    const props = {
      signIn: this.signIn,
    };

    return (
      <LoginComponent {...props} />
    );
  }
}

Login.propTypes = {
  userActions: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

Login.contextTypes = {
  router: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.users.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
