import React, { Component, PropTypes } from 'react';
import _ from 'lodash';

import UserManagementComponent from '../../components/userManagement/UserManagement';

class UserManagement extends Component {
  render() {
    const route = _.last(this.props.routes);
    return (
      <UserManagementComponent routeName={_.get(route, 'name', '')}>
        {this.props.children}
      </UserManagementComponent>
    );
  }
}

UserManagement.propTypes = {
  children: PropTypes.node.isRequired,
}

export default UserManagement;
