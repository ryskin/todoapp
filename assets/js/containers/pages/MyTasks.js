import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import * as toDoListActions from '../../actions/toDoListActions';

import MyTasksComponent from '../../components/myTasks/MyTasks';

class MyTasks extends Component {
  componentDidMount() {
    this.props.toDoListActions.loadMyTasks();
  }

  toggleCompleted = item => (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.toDoListActions.toggleToDo({
      ...item,
      completed: !item.completed,
    });
  };

  removeTask = item => (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.toDoListActions.removeToDo(item);
  };

  editTask = item => (data) => {
    this.props.toDoListActions.editToDo({
      ...item,
      name: data.name || item.name,
      description: !_.isUndefined(data.description)
        ? data.description
        : item.description,
    });
  };

  editAndRefreshTask = item => (data) => {
    this.props.toDoListActions.editAndRefreshToDo({
      ...item,
      name: data.name || item.name,
      description: !_.isUndefined(data.description)
        ? data.description
        : item.description,
    });
  };

  attachImageToTask = task => (files) => {
    this.props.toDoListActions.attachFiles(task, files);
  };

  removeImageFromTask = task => (file) => {
    this.props.toDoListActions.removeFile(task, file);
  };

  render() {
    const { users, tasks } = this.props;
    const props = {
      users,
      tasks,
      toggleCompleted: this.toggleCompleted,
      removeTask: this.removeTask,
      editTask: this.editTask,
      editAndRefreshTask: this.editAndRefreshTask,
      addSubtask: this.addSubtask,
      attachImageToTask: this.attachImageToTask,
      removeImageFromTask: this.removeImageFromTask
    };

    return (
      <MyTasksComponent {...props} />
    );
  }
}

MyTasks.propTypes = {
  toDoListActions: PropTypes.object.isRequired,
  isMobile: PropTypes.bool.isRequired,
  isTablet: PropTypes.bool.isRequired,
  tasks: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state.generalData.viewMode,
    tasks: {
      completed: _.filter(state.todos.myTasks, item => item.completed && !_.isNumber(item.parent_id)),
      incompleted: _.filter(state.todos.myTasks, item => !item.completed && !_.isNumber(item.parent_id)),
      childrens: _(state.todos.myTasks)
        .filter(item => _.isNumber(item.parent_id))
        .reduce((memo, item) => ({
          ...memo,
          [item.parent_id]: _.concat(
            _.isUndefined(memo[item.parent_id]) ? [] : memo[item.parent_id],
            item
          )
        }), {})
    },
    users: state.users.list
  }
}

function mapDispatchToProps(dispatch) {
  return {
    toDoListActions: bindActionCreators(toDoListActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyTasks);
