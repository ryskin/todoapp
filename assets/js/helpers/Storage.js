/**
 * Safari throws error if you call "(local|session)Storage.setItem(foo, bar)"
 * in private mode
 */

let storage;
let isStub = false;

try {
  localStorage.setItem('foo', 'bar');
  localStorage.removeItem('foo');

  storage = localStorage;
} catch (e) {
  const store = {};
  isStub = true;

  storage = {
    getItem: key => store[key],
    setItem: (key, value) => store[key] = value,
    removeItem: key => delete store[key],
  };
}

export function isStubMode() {
  return isStub;
}

export function getStorage(key) {
  const value = storage.getItem(key);

  if (value) {
    return JSON.parse(value);
  }

  return undefined;
}

export function setStorage(key, value) {
  if (typeof value !== 'undefined') {
    storage.setItem(key, JSON.stringify(value));
  }
}

export function removeStorage(key) {
  storage.removeItem(key);
}
