import requestTool from 'superagent-bluebird-promise';
import _ from 'lodash';
import { getStorage } from '../helpers/Storage';

export const CALL_API = 'CALL_API';

export const CHAIN_ACTION = 'CHAIN_ACTION';

function callAPI(endpoint, method = 'get', query = {}, files = []) {
  const queryStringUsing = (_.toLower(method) === 'get' || _.toLower(method) === 'head');
  let request = requestTool[method](`http://api.todoya.ru${endpoint}`)
  //let request = requestTool[method](`http://localhost:3000${endpoint}`)
    .set({
      uid: getStorage('uid'),
      'access-token': getStorage('access-token'),
      client: getStorage('client'),
      'Accept': 'application/json'
    });

  if (!files.length) {
    request = request.set('Content-Type', 'application/json');
    request = queryStringUsing ? request.query(query) : request.send(query);
  } else {
    _.map(files, (file) => {
      request.attach('files[]', file);
    });
  }

  return request;
}

function parseChainAction(config) {
  if (_.isUndefined(config) || _.isNil(config)) {
    return null;
  }

  const {ref: chainAction, needPreviouslyData} = config;

  if (typeof chainAction !== 'function') {
    throw new TypeError('Chain action `ref` property must be a function!');
  }

  return {chainAction, needPreviouslyData};
}

export default store => next => action => {
  function handleAction(act) {
    const actionAPI = act[CALL_API];

    if (_.isUndefined(actionAPI)) {
      return next(act);
    }

    const { endpoint, type, method, data, hideError, files, hiddenData } = actionAPI;

    if (typeof endpoint !== 'string') {
      throw new Error('Specify a string endpoint URL.');
    }

    const chainActionRef = parseChainAction(act[CHAIN_ACTION]);

    next({type: `${type}_REQUEST`});

    return callAPI(endpoint, method, data, files)
      .then(response => {
        const dispatchedAction = next({
          payload: response.body,
          header: response.header,
          type: `${type}_SUCCESS`,
          data,
          hiddenData,
        });

        if (chainActionRef) {
          const {chainAction, needPreviouslyData} = chainActionRef;
          return handleAction(
            chainAction(needPreviouslyData ? data : response.body)
          );
        }

        return dispatchedAction;
      }).catch(error => next({
        type: `${type}_FAILURE`,
        error: {
          ...error,
          endpoint,
          hideError,
        },
      }));
  }

  return handleAction(action);
};
//https://to-do-app-api-ryskin.c9users.io
//http://api.todoya.ru
