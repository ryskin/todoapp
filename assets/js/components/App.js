import React, { PropTypes } from 'react';

import LoaderContainer from '../containers/utils/LoaderContainer';
import ErrorsContainer from '../containers/utils/ErrorsContainer';
import RouterContainer from '../containers/utils/RouterContainer';
import PopupContainer from '../containers/utils/PopupContainer';
import DevTools from '../containers/utils/DevTools';

const App = ({ isProd, isLoadedInitData }) => {
  return isLoadedInitData ? (
    <div className="app-wrapper">
      <LoaderContainer />
      <ErrorsContainer />
      <RouterContainer />
      <PopupContainer />
			{!isProd && <DevTools />}
    </div>
  ) : (
    <div className="app-wrapper">
      <LoaderContainer />
      <ErrorsContainer />
			{!isProd && <DevTools />}
    </div>
  );
};

App.propTypes = {
  isProd: PropTypes.bool.isRequired,
  isLoadedInitData: PropTypes.bool.isRequired,
};

export default App;
