import React, {PropTypes} from 'react';
import {Field, reduxForm} from 'redux-form';

import InputField from '../utils/form/InputField';
import CustomButton from '../utils/form/CustomButton';

const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Обязательное поле';
  }

  if (!values.password) {
    errors.password = 'Обязательное поле';
  }

  return errors;
};

const Login = (props) => {
  const {handleSubmit, submitting, signIn, submitFailed} = props;

  return (
    <div className="login-view">
      <div className="login-overlay">
        <div className="login-block">
          <h1 className="title">
            Войти в аккаунт
          </h1>
          <form className="login-form" onSubmit={handleSubmit(signIn)}>
            <Field name="email" component={InputField} type="text" label="Почта" submitFailed={submitFailed} required/>
            <Field name="password" component={InputField} type="password" label="Пароль" submitFailed={submitFailed} required/>
            <CustomButton type="submit" submitting={submitting} submitFailed={submitFailed}>Войти</CustomButton>
          </form>
        </div>
      </div>
    </div>
  );
};

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  signIn: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'login',
  validate,
})(Login);
