import React from 'react';
//import RIEStatefulBase from 'riek/src/RIEStatefulBase';
import {RIEInput} from 'riek';

class RIEInputExt extends RIEInput {

  componentDidMount() {
    if (this.props.editorMode) {
      this.setState({editing: this.props.editorMode});
    }
  }

  componentWillUpdate(nextProps) {
    if (this.props.editorMode !== nextProps.editorMode) {
      this.setState({editing: nextProps.editorMode});
    }
  }
}

RIEInputExt.propTypes = {
  ...RIEInput.propTypes,
  editorMode: React.PropTypes.bool.isRequired
};

export default RIEInputExt;
