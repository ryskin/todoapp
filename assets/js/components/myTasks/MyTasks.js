import React, { PropTypes } from 'react';

import TaskList from '../common/TaskList';

const MyTasks = ({ tasks, toggleCompleted, removeTask, editTask, editAndRefreshTask,
  users, attachImageToTask, removeImageFromTask }) => {
  const properties = {
    tasks: {
      toggleCompleted,
      removeTask,
      editTask,
      editAndRefreshTask,
      users,
      attachImageToTask,
      removeImageFromTask,
      childrenTasks: tasks.childrens,
    },
  };

  return (
    <div className="mytasks-list container">
      <h4 className="title">Мои задания </h4>
      <TaskList {...properties.tasks} tasks={tasks.incompleted} title="Открытые задания" />
      <TaskList {...properties.tasks} tasks={tasks.completed} title="Выполненные задания" />
    </div>
  );
};

MyTasks.propTypes = {
  selectedProject: PropTypes.object,
  tasks: PropTypes.object.isRequired,
  toggleCompleted: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  editAndRefreshTask: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  attachImageToTask: PropTypes.func.isRequired,
  removeImageFromTask: PropTypes.func.isRequired,
};

export default MyTasks;
