import React, { PropTypes } from 'react';

import NavBarMenu from '../../containers/pages/UserManagement/Common/NavBarMenu';

const UserManagement = ({ children, routeName }) => {
  return (
    <div className="user-management">
      <NavBarMenu routeName={routeName} />
      {children}
    </div>
  );
};

UserManagement.propTypes = {
  children: PropTypes.node.isRequired,
  routeName: PropTypes.string.isRequired,
};

export default UserManagement;
