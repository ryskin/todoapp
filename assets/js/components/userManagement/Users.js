import React, { PropTypes } from 'react';
import moment from 'moment';
import _ from 'lodash';

import User from './Users/User';
import Accordion from '../common/Accordion';
import KeyValue from '../common/KeyValue';

const Users = ({ addUser, editUser, users }) => {
  const data = _.map(users, (user) => {
    const header = {
      tag: User,
      props: {
        item: user,
        editUser,
      },
    };

    const body = (
      <span>
        <KeyValue title="Почта" value={user.email} boldKey />
        <KeyValue title="Имя" value={user.name} boldKey />
        <KeyValue title="Прозвище" value={user.nickname} boldKey />
        <KeyValue title="Создан" value={moment(user.created_at).format('Do MMMM YYYY, HH:mm:ss')} boldKey />
        <KeyValue title="Последний раз обновлен" value={moment(user.updated_at).format('Do MMMM YYYY, HH:mm:ss')} boldKey />
      </span>
    );

    return {
      header,
      body,
    };
  });

  return (
    <div className="users-view container">
      <Accordion
        title="Пользователи"
        data={data}
        className="users-list"
        addItem={addUser}
      />
    </div>
  );
};

Users.propTypes = {
  addUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
};

export default Users;
