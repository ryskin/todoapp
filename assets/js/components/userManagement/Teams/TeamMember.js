import React, { PropTypes } from 'react';

import TooltipComponent from '../../common/Tooltip';

const Team = (props) => {
  const { teamMember, removeTeamMember} = props;

  return (
    <div className="team-member">
      <span className="title">{teamMember.title}</span>
      <TooltipComponent content="Удалить пользователя">
        <i className="fa fa-user-times" onClick={removeTeamMember}/>
      </TooltipComponent>
    </div>
  );
};

Team.propTypes = {
  teamMember: PropTypes.object.isRequired,
  removeTeamMember: PropTypes.func.isRequired,
};

export default Team;
