import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import TooltipComponent from '../../common/Tooltip';

function toggle(event, item, action) {
  event.stopPropagation();
  event.preventDefault();

  action(item);
}

const Team = (props) => {
  const { onClick, item, editTeam, isOpen, addTeamMember } = props;

  const className = classNames('team-view', {
    '-open': isOpen,
    '-content': _.size(item.team_members) > 0,
  });

  return (
    <div className={className} onClick={onClick}>
      <span className="title">{item.name}</span>
      <TooltipComponent content="Редактировать">
        <i
          className="fa fa-pencil"
          onClick={event => toggle(event, item, editTeam)}
        />
      </TooltipComponent>
      <TooltipComponent content="Добавить пользователя">
        <i
          className="fa fa-user-plus"
          onClick={event => toggle(event, item, addTeamMember)}
        />
      </TooltipComponent>
    </div>
  );
};

Team.propTypes = {
  onClick: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  editTeam: PropTypes.func.isRequired,
  addTeamMember: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default Team;
