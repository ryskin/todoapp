import React, { PropTypes } from 'react';
import uuid from 'node-uuid';
import _ from 'lodash';

import Team from './Teams/Team';
import TeamMember from './Teams/TeamMember';
import Accordion from '../common/Accordion';

const Teams = ({ teams, addTeam, addTeamMember, editTeam, users, removeTeamMember }) => {
  const data = _.map(teams, (team) => {
    const header = {
      tag: Team,
      props: {
        item: team,
        editTeam,
        addTeamMember,
      },
    };

    const body = (
      <span>
        {_.map(team.team_members,
          member => (
            <TeamMember
              key={uuid.v4()}
              teamMember={_.get(users, [member.id], '')}
              removeTeamMember={removeTeamMember(team, member)}
              team={team}
            />))
        }
      </span>
    );

    return {
      header,
      body,
    };
  });

  return (
    <div className="teams-view container">
      <Accordion
        title="Команды"
        data={data}
        className="team-list"
        addItem={addTeam}
      />
    </div>
  );
};

Teams.propTypes = {
  addTeam: PropTypes.func.isRequired,
  editTeam: PropTypes.func.isRequired,
  addTeamMember: PropTypes.func.isRequired,
  removeTeamMember: PropTypes.func.isRequired,
  teams: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
};

export default Teams;
