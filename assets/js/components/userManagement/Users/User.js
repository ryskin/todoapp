import React, { PropTypes } from 'react';
import classNames from 'classnames';

import TooltipComponent from '../../common/Tooltip';

function toggle(event, item, action) {
  event.stopPropagation();
  event.preventDefault();

  action(item);
}

const User = (props) => {
  const { onClick, item, editUser, isOpen } = props;

  const className = classNames('user-view -content', {
    '-open': isOpen,
  });

  return (
    <div className={className} onClick={onClick}>
      <span className="title">{item.title}</span>
      <TooltipComponent content="Редактировать">
        <i
          className="fa fa-pencil"
          onClick={event => toggle(event, item, editUser)}
        />
      </TooltipComponent>
    </div>
  );
};

User.propTypes = {
  onClick: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  editUser: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default User;
