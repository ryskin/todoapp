import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { RIETextArea } from 'riek';

const InlineEditTextArea = ({ value, editDescription }) => {

  return (
    <span className="inline-textarea-edit">
      <RIETextArea
        value={value || ''}
        change={editDescription}
        propName="description"
        className="editable-description"
      />
      <span className="static-description">{value}</span>
    </span>
  );
};

InlineEditTextArea.propTypes = {
  value: PropTypes.string,
  editDescription: PropTypes.func.isRequired,
};

InlineEditTextArea.defaultProps = {
  value: '',
};

export default InlineEditTextArea;
