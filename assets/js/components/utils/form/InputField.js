import React, { PropTypes } from 'react';
import classNames from 'classnames';

const InputField = (props) => {
  const { input, label, type, required = false,
    meta: {active, error, submitting, submitFailed} } = props;

  const className = classNames('form-input', {
    '-focus': active,
    '-error': (submitFailed && error)
  });

  return (
    <div className={className}>
      <label htmlFor={input.name}>{label}:{required ? '*' : null}</label>
      <input {...input} id={input.name} type={type} />
      {(submitFailed && error) && <span className="error">{error}</span>}
    </div>
  );
};

InputField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  type: PropTypes.string.isRequired,
  required: PropTypes.bool,
  meta: PropTypes.object.isRequired
};

export default InputField;
