import React, { PropTypes } from 'react';
import Select from 'react-select';
import classNames from 'classnames';
import _ from 'lodash';

const SelectField = (props) => {
  const {
		input,
		label,
		options,
		required,
		resetValue,
		meta: { active, error, submitting, submitFailed },
		isMultiple,
		isSimpleValuable
		} = props;

  const className = classNames('form-select', {
    '-focus': active,
    '-error': (submitFailed && error),
  });

	const singleChangeHandler = (option) => input.onChange(option.value);

	const multiChangeHandler = (options) => {
		const col = _.map(options, _.property('value'));
		input.onChange(col);
	};

	const blurHandler = () => input.onBlur(input.value);

  const selectProps = {
    ...input.value,
    name: input.name,
    id: input.name,
    value: input.value || '',
    options,
    onChange: (isMultiple ? multiChangeHandler : singleChangeHandler),
    onBlur: blurHandler,
    noResultsText: 'Результат не найден',
    placeholder: '',
		multi: isMultiple
  };

  if (!_.isUndefined(resetValue)) {
    selectProps.resetValue = resetValue;
  } else {
    selectProps.clearable = false;
  }

  return (
    <div className={className}>
      <label htmlFor={input.name}>
        {label}:{required ? '*' : null}
      </label>
      <Select {...selectProps} />
      {(submitFailed && error) && <span className="error">{error}</span>}
    </div>
  );
};

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  options: PropTypes.array.isRequired,
  required: PropTypes.bool,
  resetValue: PropTypes.object,
  meta: PropTypes.object.isRequired,
	isMultiple: PropTypes.bool.isRequired,
	isSimpleValuable: PropTypes.bool.isRequired
};

SelectField.defaultProps = {
  label: null,
  required: false,
	isMultiple: false,
	isSimpleValuable: false
};

export default SelectField;
