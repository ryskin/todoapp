import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
//import {RIEInput} from 'riek';
import RIEInputExt from '../../extended/RIEInputExt';
import TooltipComponent from '../../common/Tooltip';

class InlineEditInput extends Component {
  constructor(props) {
    super(props);

    this.state = {editorMode: false};
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(e) {
    e.stopPropagation();
    e.preventDefault();

    this.setState({editorMode: !this.state.editorMode});
  }

  render() {
    const {prefix, striked, value, editText, tooltip, children} = this.props;
    const {editorMode} = this.state;

    const className = classNames('inline-input-edit', {
      '-striked': striked,
    });

    return (
      <span className={className}>
        <span className="editable-container">
          <RIEInputExt
            value={value || ''}
            change={editText}
            propName="name"
            className="editable-title"
            editorMode={editorMode}
          />
          <span className="static-title">{prefix} {value}</span>
        </span>
        <span className="control-container">
          <TooltipComponent content={tooltip || "Редактировать"}>
            <i className="fa fa-pencil" onClick={this.clickHandler}/>
          </TooltipComponent>
          {children}
        </span>
      </span>
    );
  }
}

InlineEditInput.propTypes = {
  value: PropTypes.string,
  editText: PropTypes.func.isRequired,
  striked: PropTypes.bool,
  tooltip: PropTypes.string,
  prefix: PropTypes.any
};

InlineEditInput.defaultProps = {
  value: '',
  striked: false,
  prefix: null,
  tooltip: null
};

export default InlineEditInput;
