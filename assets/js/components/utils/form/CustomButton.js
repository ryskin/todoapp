import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

const CustomButton = (props) => {
  const className = classNames('custom-button', {
    [props.className]: !_.isUndefined(props.className),
  });

  const properties = {
    className,
    type: props.type,
    onClick: props.onClick,
		disabled: props.submitting
  };

  return (
    <button {...properties}>
      {props.children}
    </button>
  );
};

CustomButton.propTypes = {
	submitting: PropTypes.bool.isRequired,
	submitFailed: PropTypes.bool.isRequired,
  children: PropTypes.any.isRequired,
  type: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

CustomButton.defaultProps = {
  type: '',
  className: '',
  onClick: () => null,
};

export default CustomButton;
