import React, { PropTypes } from 'react';
import classNames from 'classnames';

const TextAreaField = (props) => {
  const { input, label, required = false,
    meta: { active, error, submitting, submitFailed } } = props;

  const className = classNames('form-textarea', {
    '-focus': active,
    '-error': (submitFailed && error)
  });

  return (
    <div className={className}>
      <label htmlFor={input.name}>{label}:{required ? '*' : null}</label>
      <textarea {...input} id={input.name}>{input.value}</textarea>
      {(submitFailed && error) && <span className="error">{error}</span>}
    </div>
  );
};

TextAreaField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  required: PropTypes.bool,
  meta: PropTypes.object.isRequired
};

export default TextAreaField;
