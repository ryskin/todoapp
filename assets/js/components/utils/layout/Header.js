import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

const Header = ({ user, toggleButton, toMyTasks, toTodos, toAdminstration, routeName }) => {
  const classNameSignIn = classNames('item', {
    '-lock': _.isEmpty(user),
    '-unlock': !_.isEmpty(user),
  });

  const classNameTasks = classNames('item', '-tasks', {
    '-selected': _.includes(routeName, 'my-tasks'),
  });

  const classNameProjects = classNames('item', '-projects', {
    '-selected': _.includes(routeName, 'todos'),
  });

  const classNameAdmin = classNames('item', '-secret', {
    '-selected': _.includes(routeName, 'user-management'),
  });

  return (
    <div className="page-header">
      <div className="container">
        <div className="item -title">
          {!_.isEmpty(user) ? user.email : null}
        </div>
        <div className={classNameTasks} onClick={toMyTasks}>
          <span>Мои задания</span>
        </div>
        <div className={classNameProjects} onClick={toTodos}>
          <span>Проекты</span>
        </div>
        <div className={classNameAdmin} onClick={toAdminstration}>
          <span>Управление</span>
        </div>
        <div className={classNameSignIn} onClick={toggleButton}>
          <span>{!_.isEmpty(user) ? 'Выйти' : 'Войти'}</span>
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {
  toggleButton: PropTypes.func.isRequired,
  toMyTasks: PropTypes.func.isRequired,
  toTodos: PropTypes.func.isRequired,
  toAdminstration: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  routeName: PropTypes.string.isRequired,
};

export default Header;
