import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';
import _ from 'lodash';

class TabsHeader extends Component {
  state = {
    showLinks: false,
  };

  toggleShowLinks = () => {
    this.setState({
      showLinks: !this.state.showLinks,
    });
  };

  render() {
    const { title, tabs, routeName, isMobile, isTablet } = this.props;
    const { showLinks } = this.state;

    return (
      <div className="tabs-header">
        <div className="container">
          <span className="title">
            {title}
          </span>
          {_.map(tabs, (tab, index) => {
            const isSelected = _.includes(tab.link, routeName);
            const className = classNames('tab', {
              '-selected': isSelected,
              '-show': (isMobile || isTablet) && showLinks,
            });

            const prop = {
              className,
              key: index,
            };

            if ((isMobile || isTablet) && isSelected) {
              prop.onClick = this.toggleShowLinks;
            } else {
              prop.to = tab.link;
            }

            return (
              <Link {...prop}>
                {tab.title}
              </Link>
            );
          })}
        </div>
      </div>
    );
  }
}

TabsHeader.propTypes = {
  title: PropTypes.string.isRequired,
  tabs: PropTypes.array.isRequired,
  routeName: PropTypes.string.isRequired,
};

export default TabsHeader;
