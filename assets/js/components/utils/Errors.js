import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

const Errors = ({ errors, removeError }) => {
  const className = classNames('error-view', {
    '-show': !_.isEmpty(errors),
  });

  return (
    <div className={className}>
      {_.map(errors, (item, index) =>
        (<div className="error-item alert alert-danger" key={index}>
          <p className="name"><b>Имя ошибки:</b> {item.name}</p>
          <p className="url"><b>Запрос:</b> {item.endpoint}</p>
          <p className="message"><b>Сообщение:</b> {JSON.stringify(_.get(item, 'res.body', item.message))}</p>
          <i className="fa fa-times" aria-hidden="true" onClick={() => removeError(item)}></i>
        </div>))}
    </div>
  );
};

Errors.propTypes = {
  errors: PropTypes.array.isRequired,
  removeError: PropTypes.func.isRequired
};

export default Errors;
