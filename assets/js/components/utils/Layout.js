import React, { PropTypes } from 'react';

import HeaderContainer from '../../containers/utils/Layout/HeaderContainer';

const Layout = (props) => {
  return (
    <div className="app-view">
      <HeaderContainer {...props} />
      <div className="app-container">
        {props.children}
      </div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.any,
  routeName: PropTypes.string.isRequired,
};

export default Layout;
