import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

const Loader = ({ loader }) => {
  const isEmpty = _.isEmpty(loader);
  const className = classNames('loader-view', {
    '-show': !isEmpty,
  });

  if (isEmpty) {
    document.body.classList.remove('-overflow');
  } else {
    document.body.classList.add('-overflow');
  }

  return (
    <div className={className}>
      <i className="fa fa-spinner fa-spin fa-3x fa-fw" />
    </div>
  );
};

Loader.propTypes = {
  loader: PropTypes.array.isRequired,
};

export default Loader;
