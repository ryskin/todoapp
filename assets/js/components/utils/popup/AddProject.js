import React, {Component, PropTypes} from 'react';
import {Modal} from 'react-bootstrap';
import {Field, reduxForm} from 'redux-form';
import _ from 'lodash';

import InputField from '../form/InputField';
import SelectField from '../form/SelectField';
import TextAreaField from '../form/TextAreaField';
import CustomButton from '../form/CustomButton';
import ClosePopup from './Common/ClosePopup';

export const PROJECT_FORM_NAME = 'addProject';

const AddProject = (props) => {
	const {
		statuses,
		workMode,
		categories,
		teams,
		submitting,
		submitFailed,
		handleSubmit,
		closePopupAction,
		saveProjectAction,
		removeProjectAction,
		} = props;

	return (
		<Modal show onHide={closePopupAction} className="-form">
			<ClosePopup toggle={closePopupAction} />
			<h3 className="title">Введите данные о проекте:</h3>

			<form className="addproject-form" onSubmit={handleSubmit(saveProjectAction)}>
				<Field
					name="category_id"
					component={SelectField}
					options={categories.options}
					type="text"
					label="Категория"
					submitting={submitting}
					submitFailed={submitFailed}
					resetValue={categories.resetValue}
				/>
				<Field
					name="status"
					component={SelectField}
					options={statuses}
					type="text"
					label="Статус проекта"
					submitting={submitting}
					submitFailed={submitFailed}
				/>
				<Field
					name="team_ids"
					component={SelectField}
					isMultiple={true}
					options={teams.options}
					type="text"
					label="Команда"
					submitting={submitting}
					submitFailed={submitFailed}
					resetValue={teams.resetValue}
				/>
				<Field name="name"
							 component={InputField}
							 submitting={submitting}
							 submitFailed={submitFailed}
							 type="text"
							 label="Название"
							 required  />
				<Field name="description"
							 component={TextAreaField}
							 submitting={submitting}
							 submitFailed={submitFailed}
							 label="Описание" />
				<CustomButton type="submit" submitting={submitting} submitFailed={submitFailed}>
					{_.isEqual(workMode, 'edit') ? 'Обновить' : 'Создать' }
				</CustomButton>
				{ _.isEqual(workMode, 'edit') &&
				<p className="green-link" disabled={submitting} onClick={removeProjectAction}>Удалить</p>}
			</form>
		</Modal>
	);
};

AddProject.propTypes = {
  categories: PropTypes.object.isRequired,
  teams: PropTypes.object.isRequired,
  workMode: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  statuses: PropTypes.array.isRequired,
	submitting: PropTypes.bool.isRequired,
	submitFailed: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
	closePopupAction: PropTypes.func.isRequired,
	removeProjectAction: PropTypes.func.isRequired,
	saveProjectAction: PropTypes.func.isRequired
};

export default reduxForm({
  form: PROJECT_FORM_NAME
})(AddProject);
