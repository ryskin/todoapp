import React, { PropTypes } from 'react';
import { Modal } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import _ from 'lodash';

import InputField from '../form/InputField';
import CustomButton from '../form/CustomButton';
import ClosePopup from './Common/ClosePopup';

const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Обязательное поле';
  }

  if (!values.password) {
    errors.password = 'Обязательное поле';
  } else if (_.size(values.password) < 8) {
    errors.password = 'Минимальная длина 8 символов';
  }

  return errors;
};

const AddUser = ({ data, workMode, close, handleSubmit,
  removeUser, submitData, submitFailed, submitting }) => {
  return (
    <Modal show onHide={close} className="-form">
      <ClosePopup toggle={close} />
      <h3 className="title">Введите данные о пользователе:</h3>
      <form className="adduser-form" onSubmit={handleSubmit(submitData)}>
        <Field name="email" component={InputField} type="text" label="Почта" submitFailed={submitFailed} required />
        {
          _.isEqual(workMode, 'create')
            ? (<Field name="password" component={InputField} type="password" label="Пароль" submitFailed={submitFailed} required />)
            : null
        }
        <Field name="name" component={InputField} type="text" label="Название" />
        <Field name="nickname" component={InputField} type="text" label="Прозвище" />
        <CustomButton type="submit" submitting={submitting} submitFailed={submitFailed}>
          {_.isEqual(workMode, 'edit') ? 'Обновить' : 'Создать' }
        </CustomButton>
        { _.isEqual(workMode, 'edit')
          ? (<p className="green-link" onClick={() => removeUser(data)}>Удалить</p>)
          : null}
      </form>
    </Modal>
  );
};

AddUser.propTypes = {
  workMode: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  removeUser: PropTypes.func.isRequired,
  submitData: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'addUser',
  validate,
})(AddUser);
