import React, { PropTypes } from 'react';
import { Modal } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import _ from 'lodash';

import InputField from '../form/InputField';
import CustomButton from '../form/CustomButton';
import ClosePopup from './Common/ClosePopup';

const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Обязательное поле';
  }

  return errors;
};

const AddTeam = ({ data, workMode, close, handleSubmit,
  removeTeam, submitData, submitFailed, submitting }) => {
  return (
    <Modal show onHide={close} className="-form">
      <ClosePopup toggle={close} />
      <h3 className="title">Введите данные о команде:</h3>

      <form className="addteam-form" onSubmit={handleSubmit(submitData)}>
        <Field name="name" component={InputField} type="text" label="Название" submitFailed={submitFailed} required />
        <CustomButton type="submit" submitting={submitting} submitFailed={submitFailed}>
          {_.isEqual(workMode, 'edit') ? 'Обновить' : 'Создать' }
        </CustomButton>
        { _.isEqual(workMode, 'edit')
          ? (<p className="green-link" onClick={() => removeTeam(data)}>Удалить</p>)
          : null}
      </form>
    </Modal>
  );
};

AddTeam.propTypes = {
  workMode: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  removeTeam: PropTypes.func.isRequired,
  submitData: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'addTeam',
  validate,
})(AddTeam);
