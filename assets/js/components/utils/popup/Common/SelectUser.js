import React, { PropTypes } from 'react';
import { Modal } from 'react-bootstrap';

import ClosePopup from './ClosePopup';

import UserList from './SelectUser/UserList';

const SelectUser = ({ close, title, description, users, selectUser }) => (
  <Modal show onHide={close} className="select-user">
    <ClosePopup toggle={close} />
    <h3 className="title">{title}</h3>
    <p className="description">
      {description}
    </p>
    <UserList users={users} selectUser={selectUser} />
  </Modal>
);

SelectUser.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  users: PropTypes.object.isRequired,
  selectUser: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
};

SelectUser.defaultProps = {
  description: '',
};

export default SelectUser;
