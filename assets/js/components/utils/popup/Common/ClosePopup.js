import React, { PropTypes } from 'react';

const ClosePopup = ({ toggle }) => (
  <div className="close-popup" onClick={toggle}>
    <span className="x">X</span>
    <span className="text">закрыть</span>
  </div>
);

ClosePopup.propTypes = {
  toggle: PropTypes.func.isRequired,
};

export default ClosePopup;
