import React, { PropTypes } from 'react';
import ScrollbarWrapper from 'react-scrollbar';
import _ from 'lodash';

import Image from '../../../../common/Image';
import KeyValue from '../../../../common/KeyValue';

const UserList = ({ users, selectUser }) => {
  return (
    <ScrollbarWrapper vertical className="userlist-scrollarea">
      <div className="user-list">
        {_.map(users, (item, index) =>
          (<div key={index} className="user-block" onClick={selectUser(item)}>
            <Image link={item.image} />
            <KeyValue title="Имя" value={item.nickname || item.name} className="name" vertical coloredValue bold center />
            <KeyValue title="Почта" value={item.email} className="email" vertical coloredValue bold center />
          </div>))}
      </div>
    </ScrollbarWrapper>
  );
};

UserList.propTypes = {
  users: PropTypes.object.isRequired,
  selectUser: PropTypes.func.isRequired,
};

export default UserList;
