export default (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Обязательное поле';
  }

  if (!values.status) {
    errors.status = 'Обязательное поле';
  }

  return errors;
};
