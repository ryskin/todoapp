import React, { PropTypes } from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

const TooltipComponent = ({ content, children, id }) => {
  const tooltip = (
    <Tooltip id={id}>{content}</Tooltip>
  );
  return (
    <OverlayTrigger placement="top" overlay={tooltip}>
      {children}
    </OverlayTrigger>
  );
};

TooltipComponent.propTypes = {
  children: PropTypes.any,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  id: PropTypes.string.isRequired,
};

TooltipComponent.defaultProps = {
  id: 'tooltip',
};

export default TooltipComponent;
