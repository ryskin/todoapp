import React, { PropTypes, Component } from 'react';
import { PanelGroup, Panel } from 'react-bootstrap';
import _ from 'lodash';

import TooltipComponent from './Tooltip';

class Accordion extends Component {
  state = {
    activePanel: null,
  };

  _onChange = index => (event) => {
    event.stopPropagation();
    event.preventDefault();

    const className = _.get(event, 'target.attributes[0].value');
    if (!_.isEqual(className, 'editable-title')) {
      this.setState({
        activePanel: _.isEqual(index, this.state.activePanel)
          ? null
          : index,
      });
    }
  };

  render() {
    const { title, data, className, addItem } = this.props;
    const { activePanel } = this.state;

    return (
      <div className="accordion-view">
        <div className="title">
          {title}
          { !_.isUndefined(addItem) &&
            <TooltipComponent content={ title === 'Команды' ? 'Добавить команду' : 'Добавить пользователя' }>
                <i className="fa fa-plus" onClick={addItem} />
            </TooltipComponent>}
        </div>
        <PanelGroup className={className} activeKey={activePanel} accordion>
          {
            _.map(data, (item, index) => {
              const isOpen = _.isEqual(index, activePanel);
              const headProps = {
                ...item.header.props,
                onClick: this._onChange(index),
                isOpen
              };

              const header = (<item.header.tag {...headProps} />);

              return (
                <Panel header={header} eventKey={index} key={index}>
                  {isOpen && item.body}
                </Panel>
              );
            })
          }
        </PanelGroup>
        {_.isEmpty(data) ? (<span className="empty">Задания отсутствуют</span>) : null}
      </div>
    );
  }
}

Accordion.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  className: PropTypes.string.isRequired,
  addItem: PropTypes.func,
};

export default Accordion;
