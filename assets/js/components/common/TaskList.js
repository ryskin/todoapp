import React, {Component, PropTypes } from 'react';
import {DragDropContext} from 'react-dnd';
import Html5Backend from 'react-dnd-html5-backend';
import _ from 'lodash';

import Accordion from './Accordion';
import TaskItem from '../todos/ToDoList/TaskList/TaskItem';
import InlineEditTextArea from '../utils/form/InlineEditTextArea';
import InlineEditInput from '../utils/form/InlineEditInput';
import AttachZone from './AttachZone';
import TooltipComponent from './Tooltip';

class TaskList extends Component {

  buildHeader(item, offset) {
    const {
      childrenTasks,
      toggleCompleted,
      removeTask,
      editTask,
      editAndRefreshTask,
      users,
      delegateTask
      } = this.props;

    const existSubtask = !_.isUndefined(childrenTasks[item.id]);
    const isFirstTask = (offset === 0);

    return {
      tag: TaskItem,
      props: {
        item,
        isFirstTask,
        toggleCompleted,
        removeTask,
        editTask,
        editAndRefreshTask,
        users,
        delegateTask,
        existSubtask
      }
    };
  }

  buildBody(item) {
    const {
      childrenTasks,
      toggleCompleted,
      removeTask,
      editTask,
      editAndRefreshTask,
      users,
      delegateTask,
      addSubtask,
      attachImageToTask,
      removeImageFromTask,
      } = this.props;

    return (
      <span className="panel-body-container">
        <InlineEditTextArea
          value={item.description}
          editDescription={editTask(item)}
          tooltip="Редактировать описание"
        />
				<hr />
				<AttachZone
          files={item.files}
          uploadFiles={attachImageToTask(item)}
          removeFile={removeImageFromTask(item)}
        />
				<hr />
        {addSubtask &&
        <InlineEditInput
          value=""
          editText={addSubtask(item)}
          tooltip="Добавить подзадачу"
        />}
        {
          _.map(childrenTasks[item.id], (task, i) =>
            (<TaskItem
              item={task}
              key={i * 1000}
              isFirstTask={i === 0}
              toggleCompleted={toggleCompleted}
              removeTask={removeTask}
              editTask={editTask}
              editAndRefreshTask={editAndRefreshTask}
              users={users}
              delegateTask={delegateTask}
              subtask
            />))
        }
			</span>
    );
  }

  render() {
    const {tasks, title} = this.props;

    const data = _.map(tasks, (item, offset) => ({
      header: this.buildHeader(item, offset),
      body: this.buildBody(item)
    }));

    return (
      <Accordion title={title} data={data} className="task-list"/>
    );
  };
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired,
  toggleCompleted: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  editTask: PropTypes.func.isRequired,
  editAndRefreshTask: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  childrenTasks: PropTypes.object.isRequired,
  attachImageToTask: PropTypes.func.isRequired,
  removeImageFromTask: PropTypes.func.isRequired,
  addSubtask: PropTypes.func,
  delegateTask: PropTypes.func
};

export default DragDropContext(Html5Backend)(TaskList);
