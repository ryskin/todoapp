import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

const KeyValue = (props) => {
  const className = classNames('key-value', {
    [props.className]: !_.isUndefined(props.className),
    '-bold': props.bold,
    '-coloredValue': props.coloredValue,
    '-big': props.big,
    '-boldKey': props.boldKey,
    '-vertical': props.vertical,
    '-floatLeft': props.floatLeft,
    '-center': props.center,
  });

  return (
    <div className={className}>
      <span className="key">
        {props.title}:
      </span>
      <span className="value">
        {props.value || '-//-' }
      </span>
    </div>
  );
};

KeyValue.propTypes = {
  title: PropTypes.any.isRequired,
  value: PropTypes.any,
  className: PropTypes.string,
  bold: PropTypes.bool,
  coloredValue: PropTypes.bool,
  big: PropTypes.bool,
  boldKey: PropTypes.bool,
  vertical: PropTypes.bool,
  floatLeft: PropTypes.bool,
  center: PropTypes.bool,
};

KeyValue.defaultProps = {
  value: '-//-',
  bold: false,
  coloredValue: false,
  big: false,
  boldKey: false,
  vertical: false,
  floatLeft: false,
  center: false,
};

export default KeyValue;
