import React, { PropTypes } from 'react';

const Image = (props) => {
  const link = props.link || '/assets/images/noimage.png';

  return (
    <img src={link} onClick={props.onClick} />
  );
};

Image.propTypes = {
  link: PropTypes.string,
  onClick: PropTypes.func,
};

Image.defaultProps = {
  onClick: () => null,
};

export default Image;
