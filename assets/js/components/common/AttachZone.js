import React, { PropTypes, Component } from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import Lightbox from 'react-image-lightbox';
import _ from 'lodash';

import Image from './Image';
import TooltipComponent from './Tooltip';

class AttachZone extends Component {
  state = {
    showArea: 'icon',
    index: 0,
    isOpen: false,
  };

  onDrop = (acceptedFiles) => {
    this.props.uploadFiles(acceptedFiles)
  };

  showDropZone = () => {
    this.setState({
      showArea: 'dropzone',
    });
  };

  openLightbox = index => () => {
    this.setState({
      isOpen: true,
      index,
    });
  };

  closeLightbox = () => {
    this.setState({
      isOpen: false,
    });
  };

  moveNext = () => {
    this.setState({
      index: (this.state.index + 1) % this.props.files.length,
    });
  };

  movePrev = () => {
    this.setState({
      index: (this.state.index + this.props.files.length - 1) % this.props.files.length,
    });
  };

  render() {
    const { showArea, index } = this.state;
    const { files, removeFile } = this.props;

    const className = classNames('attach-zone', {
      '-icon': _.isEqual(showArea, 'icon'),
      '-dropzone': _.isEqual(showArea, 'dropzone'),
    });

    let lightbox = null;

    if (this.state.isOpen) {
      lightbox = (
        <Lightbox
          mainSrc={`http://api.todoya.ru${files[index].url}`}
          nextSrc={`http://api.todoya.ru${files[(index + 1) % files.length].url}`}
          prevSrc={`http://api.todoya.ru${files[(index + files.length - 1) % files.length].url}`}

          onCloseRequest={this.closeLightbox}
          onMovePrevRequest={this.movePrev}
          onMoveNextRequest={this.moveNext}
        />
      );
    }

    return (
      <div className={className}>
        <TooltipComponent content="Прикрепить изображение">
          <i className="fa fa-paperclip" onClick={this.showDropZone} />
        </TooltipComponent>
        <Dropzone className="upload-image" onDrop={this.onDrop}>
          Загрузить изображение
        </Dropzone>
        <div className="images-list">
          {_.map(files, (file, i) => (
            <div className="image-item" key={i * 253}>
              <TooltipComponent content="Открепить изображение">
                <i className="fa fa-times" onClick={() => removeFile(file)} />
              </TooltipComponent>
              <Image link={`http://api.todoya.ru${file.url}`} onClick={this.openLightbox(i)} />
            </div>
          ))}
        </div>
        {lightbox}
      </div>
    );
  }
}

AttachZone.propTypes = {
  uploadFiles: PropTypes.func.isRequired,
  removeFile: PropTypes.func.isRequired,
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default AttachZone;
