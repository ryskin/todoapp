import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';

import InputField from '../../utils/form/InputField';

const AddTask = ({ handleSubmit, addTask, submitFailed }) => {
  return (
    <form onSubmit={handleSubmit(addTask)} className="add-task">
      <Field name="name" component={InputField} type="text" label="Добавить задачу" submitFailed={submitFailed} />
    </form>
  );
};

AddTask.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  addTask: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
};

const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Обязательное поле для заполнения';
  }

  return errors;
};

export default reduxForm({
  form: 'addTask',
  validate,
})(AddTask);
