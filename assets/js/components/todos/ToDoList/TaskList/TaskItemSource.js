import React, { PropTypes } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import InlineEditInput from '../../../utils/form/InlineEditInput';
import KeyValue from '../../../common/KeyValue';

import TooltipComponent from '../../../common/Tooltip';

const TaskItemSource = (props) => {
  const {
    item,
    toggleCompleted,
    removeTask,
    editTask,
    onClick,
    users,
    delegateTask,
    existSubtask,
    isOpen,
    subtask,
    connectDragSource,
    isDragging,
    connectDropTarget,
    canDrop,
    isOver
    } = props;

  const checkedClass = classNames('fa', {
    'fa-check-square-o': item.completed,
    'fa-square-o': !item.completed,
  });

  const className = classNames('task-item', {
    '-description': !!item.description || existSubtask,
    '-open': isOpen,
    '-subtask': subtask,
    '-hover': (canDrop && isOver)
  });

  const userClassName = classNames('fa', {
    'fa-user-plus': !item.assignee_id,
    'fa-user-times': !!item.assignee_id,
  });

  const userTitle = _.get(users, [item.assignee_id, 'title'], null);

  return _.flow(connectDragSource, connectDropTarget)(
    <div className={className} onClick={onClick}>
      <i className={checkedClass} onClick={toggleCompleted(item)}/>
      <div className="task-textblock">
        <InlineEditInput value={item.name} editText={editTask(item)} striked={item.completed} orderNum={item.order_num}>
          {!_.isNil(delegateTask) &&
          <TooltipComponent content="Делегирование задачи">
            <i className={userClassName} onClick={delegateTask(item)}></i>
          </TooltipComponent>}
          <TooltipComponent content="Удаление задачи">
            <i className="fa fa-times" onClick={removeTask(item)}/>
          </TooltipComponent>
        </InlineEditInput>
        {!_.isNull(userTitle) &&
        <KeyValue title="Делегировано" value={userTitle} className="user" boldKey coloredValue={!!item.assignee_id}/>}
      </div>
    </div>
  );
};

TaskItemSource.propTypes = {
  item: PropTypes.object.isRequired,
  toggleCompleted: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  editAndRefreshTask: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  users: PropTypes.object.isRequired,
  delegateTask: PropTypes.func,
  existSubtask: PropTypes.bool,
  isOpen: PropTypes.bool,
  subtask: PropTypes.bool,
  connectDragSource: PropTypes.func,
  isDragging: PropTypes.bool,
  connectDropTarget: PropTypes.func,
  canDrop: PropTypes.bool,
  isOver: PropTypes.bool
};

TaskItemSource.defaultProps = {
  existSubtask: false,
  onClick: () => null,
  subtask: false,
};

export default TaskItemSource;
