import React, {PropTypes} from 'react';
import {Panel, ListGroup} from 'react-bootstrap';

import CommentsHeader from './Comments/BlockHeader';
import CommentItem from './Comments/CommentItem';
import CommentForm from './Comments/CommentForm';

const TaskComments = ({task, comments, currentUser, addCommentAction, removeCommentAction}) => {
  return (
    <Panel
      header={<CommentsHeader {...task} />}
      footer={<CommentForm addCommentAction={addCommentAction} />}>
      <ListGroup componentClass="ul">
        {comments.map(comment =>
          <CommentItem key={comment.id}
            {...comment}
            currentUser={currentUser}
            removeCommentAction={removeCommentAction}/>
        )}
      </ListGroup>
    </Panel>
  );
};

TaskComments.propTypes = {
  task: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  removeCommentAction: PropTypes.func.isRequired
};

export default TaskComments;
