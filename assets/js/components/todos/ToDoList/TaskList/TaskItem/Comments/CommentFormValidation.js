import _ from 'lodash';

export default (values) => {
  const errors = {};

  if (_.isNil(values.content) || _.isEmpty(values.content)) {
    errors.content = 'Обязательное поле';
  }

  return errors;
};
