import React, {PropTypes} from 'react';
import {ListGroupItem, Button, Grid, Row, Col} from 'react-bootstrap';
import moment from 'moment';

const CommentItem = (props, context) => {
  const {id, content, author_id, created_at, currentUser, removeCommentAction} = props;

  const author = context.getUserInfo(author_id);
  const creationDate = moment(created_at).format('LT, ll');
  const ableToRemove = (currentUser.id === author_id);

  return (
    <ListGroupItem key={id} listItem={true}>
      <Grid>
        <Row className="show-grid">
          <Col xs={18} md={12}>{content}</Col>
        </Row>
        <Row className="show-grid">
          <Col xs={4} md={2}>
            {ableToRemove &&
            <Button bsSize="large"
                    bsStyle="link"
                    children="Удалить"
                    onClick={removeCommentAction(props)}/>}
          </Col>
          <Col xs={7} xsOffset={1} md={5} mdOffset={1}>
            {author && author.title}
          </Col>
          <Col xs={6} md={4}>{creationDate}</Col>
        </Row>
      </Grid>
    </ListGroupItem>
  );
};

CommentItem.contextTypes = {
  getUserInfo: PropTypes.func.isRequired
};

CommentItem.propTypes = {
  id: PropTypes.number.isRequired,
  content: PropTypes.string.isRequired,
  created_at: PropTypes.string.isRequired,
  currentUser: PropTypes.object.isRequired,
  removeCommentAction: PropTypes.func.isRequired
};

export default CommentItem;
