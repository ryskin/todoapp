import React, {PropTypes} from 'react';
import {ListGroupItem} from 'react-bootstrap';

const BlockHeader = ({id, name}) => {
  return (
    <ListGroupItem bsStyle="warning" listItem={true}>Комментарии</ListGroupItem>
  );
};


BlockHeader.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired
};

export default BlockHeader;
