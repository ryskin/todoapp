import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {InputGroup, Button} from 'react-bootstrap';

export const TASK_COMMENT_FORM = 'task-comment-form';

const CommentForm = ({submitting, submitFailed, addCommentAction, handleSubmit}) => {
  return (
    <form onSubmit={handleSubmit(addCommentAction)}>
      <InputGroup bsSize="large">
        <Field component="input" type="text" name="content" className="form-control" placeholder="Комментарий"/>
        <InputGroup.Button>
          <Button type="submit" bsSize="large" bsStyle="primary" children="Добавить"/>
        </InputGroup.Button>
      </InputGroup>
    </form>
  );
};

CommentForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default reduxForm({form: TASK_COMMENT_FORM})(CommentForm);
