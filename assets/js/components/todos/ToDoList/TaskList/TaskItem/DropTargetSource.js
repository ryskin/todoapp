import React, { PropTypes } from 'react';
import classNames from 'classnames';

const DropTargetSource = ({item, connectDropTarget, canDrop, isOver}) => {
  const className = classNames('task-item-drop-target', {'-hover': (canDrop && isOver)});

  return connectDropTarget(
    <div className={className}></div>
  );
};

DropTargetSource.propTypes = {
  item: PropTypes.object,
  connectDropTarget: PropTypes.func,
  canDrop: PropTypes.bool,
  isOver: PropTypes.bool
};

export default DropTargetSource;
