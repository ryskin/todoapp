import React from 'react';
import {DragSource, DropTarget} from 'react-dnd';
import _ from 'lodash';

import TaskItemSource from '../TaskItemSource';

import {
  TASK_TYPE,
  DND_DIRECTION_UP as PLACE_UP,
  DND_DIRECTION_DOWN as PLACE_DOWN,
  DND_DIRECTION_INSIDE as PLACE_INSIDE
} from '../../../../../constants/toDoConstants';

const placeInside = (draggable, target) => {
  if (target.direction === PLACE_INSIDE) {
    draggable.parent_id = target.id;
  }

  return draggable;
};

const placeNext = (draggable, target) => {
  if (target.direction === PLACE_UP) {
    draggable.order_num = target.order_num - 1;
  } else if (target.direction === PLACE_DOWN) {
    draggable.order_num = target.order_num + 1;
  }

  if (!_.isNil(draggable.parent_id)) {
    draggable.parent_id = null;
  }

  return draggable;
};

const sourceSpec = {
  beginDrag: (props) => props.item,
  endDrag: (props, monitor) => {
    if (!monitor.didDrop()) {
      return;
    }

    if (typeof props.editAndRefreshTask != 'function') {
      return;
    }

    const dropTargetItem = monitor.getDropResult();
    let draggableItem = props.item;
    draggableItem = placeNext(draggableItem, dropTargetItem);
    draggableItem = placeInside(draggableItem, dropTargetItem);

    props.editAndRefreshTask(draggableItem)(draggableItem);
  }
};

const sourceCollect = (connector, monitor) => {
  return {
    connectDragSource: connector.dragSource(),
    isDragging: monitor.isDragging()
  }
};

const targetSpec = {
  drop: (props) => ({...props.item, direction: PLACE_INSIDE}),
  canDrop: (props, monitor) => {
    const draggingItem = monitor.getItem();
    const areDifferent = (props.item.id !== draggingItem.id);
    const isParentTarget = _.isNil(props.item.parent_id);
    const isSimilarStatus = (props.item.completed === draggingItem.completed);

    return (areDifferent && isParentTarget && isSimilarStatus);
  }
};

const targetCollect = (connector, monitor) => {
  return {
    connectDropTarget: connector.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }
};

export default _.flow(
  DragSource(TASK_TYPE, sourceSpec, sourceCollect),
  DropTarget(TASK_TYPE, targetSpec, targetCollect)
)(TaskItemSource);
