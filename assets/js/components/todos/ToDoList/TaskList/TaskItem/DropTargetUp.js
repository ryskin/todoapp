import React, { PropTypes } from 'react';
import {DropTarget} from 'react-dnd';
import _ from 'lodash';

import DropTargetSource from './DropTargetSource';
import {
  TASK_TYPE,
  DND_DIRECTION_UP as PLACE_UP
} from '../../../../../constants/toDoConstants';

const targetSpec = {
  drop: (props) => ({...props.item, direction: PLACE_UP}),
  canDrop: (props, monitor) => {
    const draggingItem = monitor.getItem();
    const areDifferent = (props.item.id !== draggingItem.id);
    const isParentTarget = _.isNil(props.item.parent_id);
    const isSimilarStatus = (props.item.completed === draggingItem.completed);

    return (areDifferent && isParentTarget && isSimilarStatus);
  }
};

const targetCollect = (connector, monitor) => {
  return {
    connectDropTarget: connector.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }
};

export default DropTarget(TASK_TYPE, targetSpec, targetCollect)(DropTargetSource);
