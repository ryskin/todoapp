import React, {PropTypes} from 'react';

import DraggableSource from './TaskItem/DraggableSource';
import DropTargetUp from './TaskItem/DropTargetUp';
import DropTargetDown from './TaskItem/DropTargetDown';

const TaskItem = (props) => {
  return (
    <div className="task-item-wrapper">
      {props.isFirstTask && <DropTargetUp item={props.item}/>}
      <DraggableSource {...props} />
      <DropTargetDown item={props.item}/>
    </div>
  );
};

TaskItem.propTypes = {
  item: PropTypes.object.isRequired,
  isFirstTask: PropTypes.bool.isRequired,
  toggleCompleted: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  editAndRefreshTask: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  users: PropTypes.object.isRequired,
  delegateTask: PropTypes.func,
  existSubtask: PropTypes.bool,
  isOpen: PropTypes.bool,
  subtask: PropTypes.bool
};

export default TaskItem;
