import React, { PropTypes } from 'react';
import _ from 'lodash';

import AddTask from './ToDoList/AddTask';
import TaskList from '../common/TaskList';
import KeyValue from '../common/KeyValue';

const getLinkToProject = (selectedProject) => {
  return selectedProject.url ? (
    <a href={selectedProject.url} target="_blank">
      {selectedProject.url}
    </a>
  ) : '-//-';
};

const getProjectTeamNames = (selectedProject) => {
  return selectedProject.teams ? (
    <span>
			{_.map(selectedProject.teams, team => team.name).join(', ')}
		</span>
  ) : '-//-';
};

const ToDoList = ({ selectedProject, tasks, toggleCompleted, removeTask, editTask, editAndRefreshTask,
  addTask, users, delegateTask, addSubtask, attachImageToTask, removeImageFromTask }) => {
  const properties = {
    tasks: {
      toggleCompleted,
      removeTask,
      editTask,
      editAndRefreshTask,
      users,
      addSubtask,
      delegateTask,
      attachImageToTask,
      removeImageFromTask,
      childrenTasks: tasks.childrens,
    },
    addTask: {
      addTask,
      selectedProject,
    },
  };

  return !_.isUndefined(selectedProject)
    ? (
    <div className="todo-list">
      <KeyValue title="Проект" value={selectedProject.name} className="title" floatLeft bold coloredValue big/>
      <AddTask {...properties.addTask} />
      <KeyValue title="Статус" value={selectedProject.status} className="status" boldKey
                coloredValue={!!selectedProject.status}/>
      <KeyValue title="Команды" value={getProjectTeamNames(selectedProject)} className="url" boldKey
                coloredValue={!!selectedProject.teams.length}/>
      <KeyValue title="Описание" value={selectedProject.description} className="description" boldKey
                vertical={!!selectedProject.description}/>
      <TaskList {...properties.tasks} tasks={tasks.incompleted} title="Открытые задания"/>
      <TaskList {...properties.tasks} tasks={tasks.completed} title="Выполненные задания"/>
    </div>
  ) : null;
};

ToDoList.propTypes = {
  selectedProject: PropTypes.object,
  tasks: PropTypes.object.isRequired,
  toggleCompleted: PropTypes.func.isRequired,
  removeTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  editAndRefreshTask: PropTypes.func.isRequired,
  addTask: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  delegateTask: PropTypes.func.isRequired,
  addSubtask: PropTypes.func.isRequired,
  attachImageToTask: PropTypes.func.isRequired,
  removeImageFromTask: PropTypes.func.isRequired,
};

export default ToDoList;
