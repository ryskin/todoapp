import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import ProjectItem from './ProjectList/ProjectItem';
import TooltipComponent from '../../common/Tooltip';

class ProjectList extends Component {
  state = {
    showAllProjects: false,
  };

  toggleShow = () => {
    this.setState({
      showAllProjects: !this.state.showAllProjects,
    });
  };

  render() {
    const { categoryId, list, categories, editProject } = this.props;
    const { showAllProjects } = this.state;

    const projects = _.filter(list, item => (
      showAllProjects || _.isEqual(item.status, 'active')
    ));

    const showClassName = classNames('fa', {
      'fa-eye': !showAllProjects,
      'fa-eye-slash': showAllProjects,
    });

    return (
      <div className="project-list">
        <div className="title">
          {_.get(categories, `[${categoryId}].name`, '-//-') }
          <TooltipComponent content={!showAllProjects ? "Показать все проекты" : "Скрыть неактивные проекты"}>
            <i className={showClassName} onClick={this.toggleShow}></i>
          </TooltipComponent>
        </div>
        {_.map(projects, (item, index) => (
          <ProjectItem
            editProject={editProject}
            item={item}
            key={`${categoryId}-${index}`}
          />
        ))}
      </div>
    );
  }
}

ProjectList.propTypes = {
  categoryId: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  categories: PropTypes.object.isRequired,
  editProject: PropTypes.func.isRequired,
};

export default ProjectList;
