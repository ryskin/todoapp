import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';
import _ from 'lodash';

import TooltipComponent from '../../../common/Tooltip';

function toggleEdit(event, item, editProject) {
  event.stopPropagation();
  event.preventDefault();

  editProject(item);
}

const ProjectItem = ({ item, editProject }) => {
  const className = classNames('project-item', {
    '-selected': item.selected,
    '-completed': _.isEqual(item.status, 'completed'),
    '-hold': _.isEqual(item.status, 'on_hold'),
    '-droped': _.isEqual(item.status, 'droped'),
  });

  const link = `/todos/${item.id}`;

  return (
    <Link to={link} className={className}>
      <span className="title">{item.name}</span>
      <TooltipComponent content="Редактировать проект">
        <i className="fa fa-pencil" onClick={event => toggleEdit(event, item, editProject)} />
      </TooltipComponent>
    </Link>
  );
};

ProjectItem.propTypes = {
  item: PropTypes.object.isRequired,
  editProject: PropTypes.func.isRequired,
};

export default ProjectItem;
