import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';

import ProjectsContainer from '../../containers/pages/Todos/Projects';
import ToDoListContainer from '../../containers/pages/Todos/ToDoList';

const Todos = (props) => {
  return (
    <div className="to-do container">
      {(props.isMobile || props.isTablet) && !_.isUndefined(props.params.projectId)
        ? (
        <Link to="/todos" className="back-view">
          <i className="fa fa-caret-left"/>
          <span className="title">Вернуться к проектам</span>
        </Link>)
        : (<ProjectsContainer {...props} />)}
      <ToDoListContainer />
    </div>
  );
};

Todos.propTypes = {
  params: PropTypes.object,
};

export default Todos;

