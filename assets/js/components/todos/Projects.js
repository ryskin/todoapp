import React, { PropTypes } from 'react';
import _ from 'lodash';

import ProjectList from './Projects/ProjectList';
import TooltipComponent from '../common/Tooltip';

const Projects = ({ projects, categories, addProject, editProject }) => (
  <div className="project-view">
    <h4 className="title">
      Проекты
      <TooltipComponent content="Добавить проект">
        <i className="fa fa-plus" onClick={addProject} />
      </TooltipComponent>
    </h4>
    {
      _.map(projects, (list, categoryId) =>
        (<ProjectList
          editProject={editProject}
          list={list}
          categoryId={categoryId}
          categories={categories}
          key={categoryId}
        />))
    }
  </div>
);

Projects.propTypes = {
  projects: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired,
  addProject: PropTypes.func.isRequired,
  editProject: PropTypes.func.isRequired,
};

export default Projects;
