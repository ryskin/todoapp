import * as constants from '../constants/popupConstants';

export function openPopup(data) {
  return {
    type: constants.OPEN_POPUP,
    payload: data,
  };
}

export function closePopup() {
  return {
    type: constants.CLOSE_POPUP,
  };
}
