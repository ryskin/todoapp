import _ from 'lodash';

import * as constants from '../constants/userConstants';

import { CALL_API } from '../middleware/API';

export function signIn(data) {
  return {
    [CALL_API]: {
      type: constants.SIGN_IN,
      endpoint: '/auth/sign_in',
      method: 'post',
      data,
    },
  };
}

export function loadUser(data) {
  return {
    [CALL_API]: {
      type: constants.LOAD_USER,
      endpoint: '/auth/validate_token',
      method: 'get',
      data,
      hideError: true,
    },
  };
}

export function loadUserList() {
  return {
    [CALL_API]: {
      type: constants.LOAD_USER_LIST,
      endpoint: '/users',
      method: 'get',
    },
  };
}

export function signOut() {
  return {
    [CALL_API]: {
      type: constants.SIGN_OUT,
      endpoint: '/auth/sign_out',
      method: 'delete',
    },
  };
}

export function loadTeams() {
  return {
    [CALL_API]: {
      type: constants.LOAD_TEAMS,
      endpoint: '/teams.json',
      method: 'get',
    },
  };
}

export function loadTeamMembers() {
  return {
    [CALL_API]: {
      type: constants.LOAD_TEAM_MEMBERS,
      endpoint: '/team_members.json',
      method: 'get',
    },
  };
}

export function addTeam(teamData) {
  return {
    [CALL_API]: {
      type: constants.ADD_TEAM,
      endpoint: '/teams.json',
      method: 'post',
      data: {data: {attributes: teamData}}
    },
  };
}

export function editTeam(teamData) {
  return {
    [CALL_API]: {
      type: constants.EDIT_TEAM,
      endpoint: `/teams/${teamData.id}.json`,
      method: 'put',
      data: {data: {attributes: teamData}}
    },
  };
}

export function removeTeam(teamData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_TEAM,
      endpoint: `/teams/${teamData.id}.json`,
      method: 'delete',
      data: {data: {attributes: teamData}}
    },
  };
}

export function addUser(userData) {
  return {
    [CALL_API]: {
      type: constants.ADD_USER,
      endpoint: '/users',
      method: 'post',
      data: {data: {attributes: userData}}
    },
  };
}

export function editUser(userData) {
  return {
    [CALL_API]: {
      type: constants.EDIT_USER,
      endpoint: `/users/${userData.id}`,
      method: 'put',
      data: {data: {attributes: userData}}
    },
  };
}

export function removeUser(userData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_USER,
      endpoint: `/users/${userData.id}`,
      method: 'delete',
      data: {data: {attributes: userData}}
    },
  };
}

export function addTeamMember(teamMemberData) {
  return {
    [CALL_API]: {
      type: constants.ADD_TEAM_MEMBER,
      endpoint: `/team_members.json`,
      method: 'post',
      data: teamMemberData
    },
  };
}

export function removeTeamMember(teamMemberData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_TEAM_MEMBER,
      endpoint: `/team_members/${teamMemberData.id}.json`,
      method: 'delete',
      data: teamMemberData
    },
  };
}
