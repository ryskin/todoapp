import * as constants from '../constants/errorConstants';

export function removeError(data) {
  return {
    type: constants.REMOVE_ERROR,
    data,
  };
}