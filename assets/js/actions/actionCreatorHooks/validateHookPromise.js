import {SubmissionError} from 'redux-form';
import _ from 'lodash';

export default (validateCb, nextCb = null) => {
	if (typeof validateCb !== 'function') {
		throw new TypeError('Second argument must be a function!');
	}

	if (nextCb && typeof nextCb !== 'function') {
		throw new TypeError('Third argument must be a function!');
	}

	return (data) => {
		return new Promise((resolve, reject) => {
			const errors = validateCb(data);

			if (!_.isEmpty(errors)) {
				reject(errors);
			} else {
				resolve(data);
			}
		}).then(data => nextCb(data))
			.catch(errors => {
				throw new SubmissionError(errors);
			});
	}
};
