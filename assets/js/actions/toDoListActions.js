import * as constants from '../constants/toDoConstants';

import { CALL_API, CHAIN_ACTION } from '../middleware/API';

export function loadTasks() {
  return {
    [CALL_API]: {
      type: constants.LOAD_TASKS,
      endpoint: '/tasks',
      method: 'get',
    },
  };
}

export function sortToDoList(todoListData) {
  return {
    [CALL_API]: {
      type: constants.SORT_TO_DO_LIST,
      endpoint: '/tasks/sort',
      method: 'post',
      data: {data: {attributes: todoListData}}
    },
  };
}

export function toggleToDo(todoListData) {
  return {
    [CALL_API]: {
      type: constants.TOGGLE_TO_DO,
      endpoint: `/tasks/${todoListData.id}`,
      method: 'put',
      data: {data: {attributes: todoListData}}
    },
  };
}

export function addToDo(todoListData) {
  return {
    [CALL_API]: {
      type: constants.ADD_TO_DO,
      endpoint: '/tasks',
      method: 'post',
      data: {data: {attributes: todoListData}}
    },
  };
}

export function removeToDo(todoListData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_TO_DO,
      endpoint: `/tasks/${todoListData.id}`,
      method: 'delete',
      data: {data: {attributes: todoListData}}
    },
  };
}

export function editToDo(todoListData) {
  return {
    [CALL_API]: {
      type: constants.EDIT_TO_DO,
      endpoint: `/tasks/${todoListData.id}`,
      method: 'put',
      data: {data: {attributes: todoListData}}
    },
  };
}

export function delegateTask(todoListdata) {
  return {
    [CALL_API]: {
      type: constants.DELEGATE_TASK,
      endpoint: `/tasks/${todoListdata.id}`,
      method: 'put',
      data: {data: {attributes: todoListdata}}
    },
  };
}

export function attachFiles(task, files) {
  return {
    [CALL_API]: {
      type: constants.ATTACH_FILES,
      endpoint: `/tasks/${task.id}/add_files.json`,
      method: 'post',
      hiddenData: task,
      data: {data: {attributes: files}}
    },
  };
}

export function removeFile(task, file) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_FILE,
      endpoint: `/tasks/${task.id}/delete_file.json`,
      method: 'post',
      data: {data: {attributes: file}},
      hiddenData: {
        id: task.id
      }
    },
  };
}

export function loadMyTasks() {
  return {
    [CALL_API]: {
      type: constants.LOAD_MY_TASKS,
      endpoint: '/home/my_tasks.json',
      method: 'get',
    },
  };
}

export function editAndRefreshToDo(data) {
  return {
    [CALL_API]: {
      type: constants.EDIT_TO_DO,
      endpoint: `/tasks/${data.id}`,
      method: 'put',
      data: {data: {attributes: data}}
    },
    [CHAIN_ACTION]: {
      ref: loadTasks,
      needPreviouslyData: false
    }
  };
}

export function loadComments(commentData) {
  const taskId = commentData.commentable_id || commentData.id;

  return {
    [CALL_API]: {
      type: constants.LOAD_COMMENTS,
      endpoint: `/tasks/${taskId}/comments.json`,
      method: 'get',
      data: {data: {attributes: commentData}}
    }
  };
}

export function addComment(commentData) {
  return {
    [CALL_API]: {
      type: constants.ADD_COMMENT,
      //endpoint: `/tasks/${data.commentable_id}/comments.json`,
      endpoint: `/tasks/${commentData.commentable_id}/comments.json`,
      method: 'post',
      data: {data: {attributes: commentData}}
    }
  };
}

export function removeComment(commentData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_COMMENT,
//      endpoint: `/tasks/${commentData.commentable_id}/comments/${data.id}.json`,
      endpoint: `/tasks/comments/${commentData.id}.json`,
      method: 'delete',
      data: {data: {attributes: commentData}}
    }
  };
}
