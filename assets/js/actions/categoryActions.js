import * as constants from '../constants/categoryConstants';

import { CALL_API } from '../middleware/API';

export function loadCategories() {
  return {
    [CALL_API]: {
      type: constants.LOAD_CATEGORIES,
      endpoint: '/categories',
      method: 'get',
    },
  };
}

export function addCategory(categoryData) {
  return {
    [CALL_API]: {
      type: constants.ADD_CATEGORY,
      endpoint: '/categories',
      method: 'post',
      data: {data: {attributes: categoryData}}
    },
  };
}

export function editCategory(categoryData) {
  return {
    [CALL_API]: {
      type: constants.EDIT_CATEGORY,
      endpoint: `/categories/${categoryData.id}`,
      method: 'put',
      data: {data: {attributes: categoryData}}
    },
  };
}

export function removeCATEGORY(categoryData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_CATEGORY,
      endpoint: `/categories/${categoryData.id}`,
      method: 'delete',
      data: {data: {attributes: categoryData}}
    },
  };
}
