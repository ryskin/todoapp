import * as constants from '../constants/projectConstants';

import { CALL_API, CHAIN_ACTION } from '../middleware/API';

export function loadProjects(projectData) {
  return {
    [CALL_API]: {
      type: constants.LOAD_PROJECTS,
      endpoint: '/projects.json',
      method: 'get',
      data: {data: {attributes: projectData}}
    },
  };
}

export function loadStatuses() {
  return {
    [CALL_API]: {
      type: constants.LOAD_PROJECT_STATUSES,
      endpoint: '/projects/statuses',
      method: 'get',
    },
  };
}

export function selectProject(data) {
  return {
    type: constants.SELECT_PROJECT,
    payload: data,
  };
}

export function addProject(projectData) {
  return {
    [CALL_API]: {
      type: constants.ADD_PROJECT,
      endpoint: '/projects',
      method: 'post',
      data: {data: {attributes: projectData}}
    },
    [CHAIN_ACTION]: {
      ref: loadProjects,
      needPreviouslyData: false
    }
  };
}

export function editProject(projectData) {
  return {
    [CALL_API]: {
      type: constants.EDIT_PROJECT,
      endpoint: `/projects/${projectData.id}`,
      method: 'put',
      data: {data: {attributes: projectData}}
    },
    [CHAIN_ACTION]: {
      ref: loadProjects,
      needPreviouslyData: false
    }
  };
}

export function removeProject(projectData) {
  return {
    [CALL_API]: {
      type: constants.REMOVE_PROJECT,
      endpoint: `/projects/${projectData.id}`,
      method: 'delete',
      data: {data: {attributes: projectData}}
    },
    [CHAIN_ACTION]: {
      ref: loadProjects,
      needPreviouslyData: false
    }
  };
}
