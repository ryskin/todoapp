import * as constants from '../constants/generalConstants';

export function updateViewMode(data) {
  return {
    type: constants.UPDATE_VIEW_MODE,
    payload: data,
  };
}
