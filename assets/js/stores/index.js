import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import API from '../middleware/API';

import reducers from '../reducers';
import DevTools from '../containers/utils/DevTools';

let middleware = applyMiddleware(thunkMiddleware, API);

export default (isProd) => {
  const enhancer = isProd ? middleware : compose(middleware, DevTools.instrument());

  return createStore(reducers, enhancer);
}
