var webpack = require('webpack');

module.exports = {
  entry: [
    './assets/js/index.js',
    './assets/css/importer.less'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js',
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1'],
      },
    }, {
      test: /\.less$/,
      loader: 'style!css!less!autoprefixer-loader?{browsers:["last 3 versions", "safari 5", "ie 10", "opera 12.1", "ios 6", "android 4"]}',
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader?root=.',
    }],
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: './',
  },
  devtool: '#source-map',
};
