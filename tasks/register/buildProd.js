module.exports = function (gulp, plugins) {
  gulp.task('buildProd', function(cb) {
    plugins.sequence(
      'compileAssets',
      'concat:js',
      'concat:css',
      'cssnano:dist',
      'uglify:dist',
      'sails-linker-gulp:prodAssetsRelative',
      'images:prod',
      'clean:build',
      cb
    );
  });
};
