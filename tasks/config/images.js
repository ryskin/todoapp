/**
 * Copies images to .tmp/images after compressing them.
 *
 * ---------------------------------------------------------------
 *
 *
 */
module.exports = function(gulp, plugins, growl) {
  gulp.task('images:prod', function() {
    return gulp.src('./assets/images/**/*')
      .pipe(plugins.imagemin())
      .pipe(gulp.dest('.tmp/public/assets/images'))
      .pipe(plugins.if(growl, plugins.notify({ message: 'Images task complete' })));
  });
};
