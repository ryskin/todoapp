/**
 * Run predefined tasks whenever watched file patterns are added, changed or deleted.
 *
 * ---------------------------------------------------------------
 *
 * Watch for changes on
 * - files in the `assets` folder
 * - the `tasks/pipeline.js` file
 * and re-run the appropriate tasks.
 *
 *
 */
const livereload = require('gulp-livereload');

module.exports = function (gulp, plugins, growl) {
  gulp.task('watch:api', () =>
		// Watch Style files
		gulp.watch('api/**/*', ['syncAssets'])
				.on('change', (file) => { livereload.changed(file.path); })
	);

  gulp.task('watch:assets', () =>
		// Watch assets
		gulp.watch(['assets/**/*', 'tasks/pipeline.js'], ['syncAssets'])
      .on('change', (file) => { livereload.changed(file.path); })
	);

  gulp.task('watch:less', () =>
    gulp.watch(['assets/**/*.less'], ['less:dev'])
      .on('change', (file) => { livereload.changed(file.path); })
  );

  gulp.task('watch:react', () =>
    gulp.watch(['assets/js/**/*'], ['react:dev'])
      .on('change', (file) => { livereload.changed(file.path); })
  );
};
