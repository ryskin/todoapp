/**
 * Minify files with UglifyJS.
 *
 * ---------------------------------------------------------------
 *
 * Minifies client-side javascript `assets`.
 *
 */



module.exports = function (gulp, plugins, growl) {

  function handleErrors(err) {
    console.error(`${err.message}\n${err.codeFrame}`);
    if (gulp.seq.indexOf('buildProd') === -1) {
      this.emit('end');
    } else {
      this.emit('error');
    }
  }

  gulp.task('uglify:dist', function () {
    return gulp.src('.tmp/public/concat/production.js')
      .pipe(plugins.rename({ suffix: '.min' }))
      .pipe(plugins.uglify({ mangle: false, compress: { drop_console: true } }).on('error', handleErrors))
      .pipe(plugins.rev())
      .pipe(gulp.dest('.tmp/public/min'))
      .pipe(plugins.if(growl, plugins.notify({ message: 'uglify dist task complete' })));
  });
};
