/**
 * Concatenate files.
 *
 * ---------------------------------------------------------------
 *
 * Concatenates files javascript and css from a defined array. Creates concatenated files in
 * .tmp/public/contact directory
 *
 */

module.exports = function(gulp, plugins, growl) {
  gulp.task('concat:js', function() {
    return gulp.src('./.tmp/public/js/*.js')
        .pipe(plugins.concat('production.js'))
        .pipe(gulp.dest('./.tmp/public/concat'))
        .pipe(plugins.if(growl, plugins.notify({ message: 'Concatenate Scripts task complete' })));
  });

  gulp.task('concat:css', function() {
    return gulp.src(['./.tmp/public/dependencies/*.min.css', './.tmp/public/styles/importer-*.css'])
      .pipe(plugins.concat('production.css'))
      .pipe(gulp.dest('./.tmp/public/concat'))
      .pipe(plugins.if(growl, plugins.notify({ message: 'Concatenate CSS task complete' })));
  });
};

