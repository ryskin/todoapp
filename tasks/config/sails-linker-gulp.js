/**
 * Autoinsert script tags (or other filebased tags) in an html file.
 *
 * ---------------------------------------------------------------
 *
 * Automatically inject <script> tags for javascript files and <link> tags
 * for css files. Also automatically links an output file containing precompiled
 * templates using a <script> tag.
 *
 * For usage docs see (the original):
 * 		https://github.com/Zolmeister/grunt-sails-linker
 *
 */

module.exports = function(gulp, plugins, growl) {
    gulp.task('sails-linker-gulp:prodAssetsRelative', function() {
        return gulp.src('.tmp/public/**/*.html')
            .pipe(plugins.linker({
                scripts: ['.tmp/public/min/production-*.min.js'],
                startTag: '<!--SCRIPTS-->',
                endTag: '<!--SCRIPTS END-->',
                fileTmpl: '<script src="%s"></script>',
                appRoot: '.tmp/public',
                relative: false,
            }))
            .pipe(plugins.linker({
                scripts: ['.tmp/public/min/production-*.min.css'],
                startTag: '<!--STYLES-->',
                endTag: '<!--STYLES END-->',
                fileTmpl: '<link rel="stylesheet" href="%s">',
                appRoot: '.tmp/public',
                relative: false,
            }))
            .pipe(gulp.dest('.tmp/public/'))
            .pipe(plugins.if(growl, plugins.notify({ message: 'sails-linker-gulp:prodAssetsRelative task complete' })));
    });
};
