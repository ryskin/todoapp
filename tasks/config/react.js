const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const watchify = require('watchify');
const babelify = require('babelify');
const gutil = require('gulp-util');
const envify = require('envify/custom');

module.exports = function (gulp, plugins, growl) {
  const buildDir = '.tmp/public/js';
  const scriptsDir = 'assets/js';
  const entryFile = `${scriptsDir}/index.js`;

  const options = {
    cache: {},
    packageCache: {},
    fullPaths: true,
    entries: [entryFile],
    debug: true,
  };

  const bundler = browserify(options).transform('babelify', {
    presets: [
      'stage-0', 'es2015', 'react',
    ],
    plugins: [
      'transform-class-properties',
      'transform-strict-mode',
      'transform-object-rest-spread',
    ],
  })
  .transform(envify({
    NODE_ENV: process.env.NODE_ENV
  }));

  function bundle() {
    gutil.log('Start JS building process...');
    return bundler.bundle()
      .on('error', gutil.log)
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(plugins.sourcemaps.init({ loadMaps: true }))
      .pipe(plugins.rev())
      .pipe(plugins.sourcemaps.write('./'))
      .pipe(gulp.dest(`${buildDir}/`))
      .on('end', () => { gutil.log('Js building complete'); });
  }

  gulp.task('react:dev', () => bundle());

  gulp.task('react:watch', () => {
    const watcher = watchify(bundler);
    watcher.on('update', bundle);
    watcher.on('log', gutil.log);

    return bundle();
  });

  gulp.task('react:build', () => bundle());
};

