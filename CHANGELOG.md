## [v0.3.2]
> 2017-01-30

- **Feature:** Tooltips for icons
- **Feature:** Commentable tasks
- **Feature:** Ability to map a several teams to project
- **Feature:** Drag&Drop of subtasks
- **Feature:** Drag&Drop of tasks
- **Feature:** Add .editorconfig
- **Bugfix:** Change icon for add subtask
- **Bugfix:** Change width my tasks

[v0.3.2]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.3.2%0Dv0.3.1

## [v0.3.1]
> 2017-01-09

- **Bugfix:** integrate webpack dev with gulp prod

[v0.3.1]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.3.1%0Dv0.3.0

## [v0.3.0]
> 2016-12-25

- **Feature:** Add action attach files
- **Feature:** Add page my tasks `/my-tasks`

[v0.3.0]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.3.0%0Dv0.2.0

## [v0.2.0]
> 2016-12-10

- **Feature:** Add load init data
- **Feature:** Add navigation bar
- **Feature:** Update package.json
- **Feature:** Migrate `viewMode` to `store`
- **Feature:** Unification accordion
- **Feature:** Add page team members `/user-management/teams`
- **Feature:** Add page users `/user-management/users`

[v0.2.0]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.2.0%0Dv0.1.2

## [v0.1.2]
> 2016-11-14

- **Bugfix:** [gulp-linker] change relative path to absolute

[v0.1.2]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.1.2%0Dv0.1.1

## [v0.1.1]:
> 2016-11-11

- **Bugfix:** Change submit function name from `submit` to `submitData`

[v0.1.1]: https://bitbucket.org/ryskin/todoapp/branches/compare/v0.1.1%0Dv0.1.0

## [v0.1.0]
> 2016-11-11

- **Feature:** Add page `/todos/:id`
- **Feature:** Add Spiner
- **Feature:** Add handle errors
- **Feature:** Add login page `/login` with authorization logic
- **Feature:** Add gulp job for prod
- **Feature:** Create responsive version

[v0.1.0]: https://bitbucket.org/ryskin/todoapp/commits/tag/v0.1.0
